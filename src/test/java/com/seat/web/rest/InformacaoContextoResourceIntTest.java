package com.seat.web.rest;

import com.seat.SeatApp;

import com.seat.domain.InformacaoContexto;
import com.seat.repository.InformacaoContextoRepository;
import com.seat.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the InformacaoContextoResource REST controller.
 *
 * @see InformacaoContextoResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SeatApp.class)
public class InformacaoContextoResourceIntTest {

    private static final String DEFAULT_NOME_ORGANIZACAO = "AAAAAAAAAA";
    private static final String UPDATED_NOME_ORGANIZACAO = "BBBBBBBBBB";

    private static final Integer DEFAULT_NUMERO_COLABORADORES = 1;
    private static final Integer UPDATED_NUMERO_COLABORADORES = 2;

    private static final Integer DEFAULT_NUMERO_PROJETOS = 1;
    private static final Integer UPDATED_NUMERO_PROJETOS = 2;

    private static final String DEFAULT_AREA_ATUACAO = "AAAAAAAAAA";
    private static final String UPDATED_AREA_ATUACAO = "BBBBBBBBBB";

    private static final String DEFAULT_CERTIFICACAO = "AAAAAAAAAA";
    private static final String UPDATED_CERTIFICACAO = "BBBBBBBBBB";

    @Autowired
    private InformacaoContextoRepository informacaoContextoRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restInformacaoContextoMockMvc;

    private InformacaoContexto informacaoContexto;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        InformacaoContextoResource informacaoContextoResource = new InformacaoContextoResource(informacaoContextoRepository, null);
        this.restInformacaoContextoMockMvc = MockMvcBuilders.standaloneSetup(informacaoContextoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static InformacaoContexto createEntity(EntityManager em) {
        InformacaoContexto informacaoContexto = new InformacaoContexto();
        informacaoContexto.setNomeOrganizacao(DEFAULT_NOME_ORGANIZACAO);
        informacaoContexto.setNumeroColaboradores(DEFAULT_NUMERO_COLABORADORES);
        informacaoContexto.setNumeroProjetos(DEFAULT_NUMERO_PROJETOS);
        informacaoContexto.setAreaAtuacao(DEFAULT_AREA_ATUACAO);
        informacaoContexto.setCertificacao(DEFAULT_CERTIFICACAO);
        return informacaoContexto;
    }

    @Before
    public void initTest() {
        informacaoContexto = createEntity(em);
    }

    @Test
    @Transactional
    public void createInformacaoContexto() throws Exception {
        int databaseSizeBeforeCreate = informacaoContextoRepository.findAll().size();

        // Create the InformacaoContexto
        restInformacaoContextoMockMvc.perform(post("/api/informacao-contextos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(informacaoContexto)))
            .andExpect(status().isCreated());

        // Validate the InformacaoContexto in the database
        List<InformacaoContexto> informacaoContextoList = informacaoContextoRepository.findAll();
        assertThat(informacaoContextoList).hasSize(databaseSizeBeforeCreate + 1);
        InformacaoContexto testInformacaoContexto = informacaoContextoList.get(informacaoContextoList.size() - 1);
        assertThat(testInformacaoContexto.getNomeOrganizacao()).isEqualTo(DEFAULT_NOME_ORGANIZACAO);
        assertThat(testInformacaoContexto.getNumeroColaboradores()).isEqualTo(DEFAULT_NUMERO_COLABORADORES);
        assertThat(testInformacaoContexto.getNumeroProjetos()).isEqualTo(DEFAULT_NUMERO_PROJETOS);
        assertThat(testInformacaoContexto.getAreaAtuacao()).isEqualTo(DEFAULT_AREA_ATUACAO);
        assertThat(testInformacaoContexto.getCertificacao()).isEqualTo(DEFAULT_CERTIFICACAO);
    }

    @Test
    @Transactional
    public void createInformacaoContextoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = informacaoContextoRepository.findAll().size();

        // Create the InformacaoContexto with an existing ID
        informacaoContexto.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restInformacaoContextoMockMvc.perform(post("/api/informacao-contextos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(informacaoContexto)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<InformacaoContexto> informacaoContextoList = informacaoContextoRepository.findAll();
        assertThat(informacaoContextoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllInformacaoContextos() throws Exception {
        // Initialize the database
        informacaoContextoRepository.saveAndFlush(informacaoContexto);

        // Get all the informacaoContextoList
        restInformacaoContextoMockMvc.perform(get("/api/informacao-contextos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(informacaoContexto.getId().intValue())))
            .andExpect(jsonPath("$.[*].nomeOrganizacao").value(hasItem(DEFAULT_NOME_ORGANIZACAO.toString())))
            .andExpect(jsonPath("$.[*].numeroColaboradores").value(hasItem(DEFAULT_NUMERO_COLABORADORES)))
            .andExpect(jsonPath("$.[*].numeroProjetos").value(hasItem(DEFAULT_NUMERO_PROJETOS)))
            .andExpect(jsonPath("$.[*].areaAtuacao").value(hasItem(DEFAULT_AREA_ATUACAO.toString())))
            .andExpect(jsonPath("$.[*].certificacao").value(hasItem(DEFAULT_CERTIFICACAO.toString())));
    }

    @Test
    @Transactional
    public void getInformacaoContexto() throws Exception {
        // Initialize the database
        informacaoContextoRepository.saveAndFlush(informacaoContexto);

        // Get the informacaoContexto
        restInformacaoContextoMockMvc.perform(get("/api/informacao-contextos/{id}", informacaoContexto.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(informacaoContexto.getId().intValue()))
            .andExpect(jsonPath("$.nomeOrganizacao").value(DEFAULT_NOME_ORGANIZACAO.toString()))
            .andExpect(jsonPath("$.numeroColaboradores").value(DEFAULT_NUMERO_COLABORADORES))
            .andExpect(jsonPath("$.numeroProjetos").value(DEFAULT_NUMERO_PROJETOS))
            .andExpect(jsonPath("$.areaAtuacao").value(DEFAULT_AREA_ATUACAO.toString()))
            .andExpect(jsonPath("$.certificacao").value(DEFAULT_CERTIFICACAO.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingInformacaoContexto() throws Exception {
        // Get the informacaoContexto
        restInformacaoContextoMockMvc.perform(get("/api/informacao-contextos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateInformacaoContexto() throws Exception {
        // Initialize the database
        informacaoContextoRepository.saveAndFlush(informacaoContexto);
        int databaseSizeBeforeUpdate = informacaoContextoRepository.findAll().size();

        // Update the informacaoContexto
        InformacaoContexto updatedInformacaoContexto = informacaoContextoRepository.findOne(informacaoContexto.getId());
        updatedInformacaoContexto.setNomeOrganizacao(UPDATED_NOME_ORGANIZACAO);
        updatedInformacaoContexto.setNumeroColaboradores(UPDATED_NUMERO_COLABORADORES);
        updatedInformacaoContexto.setNumeroProjetos(UPDATED_NUMERO_PROJETOS);
        updatedInformacaoContexto.setAreaAtuacao(UPDATED_AREA_ATUACAO);
        updatedInformacaoContexto.setCertificacao(UPDATED_CERTIFICACAO);

        restInformacaoContextoMockMvc.perform(put("/api/informacao-contextos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedInformacaoContexto)))
            .andExpect(status().isOk());

        // Validate the InformacaoContexto in the database
        List<InformacaoContexto> informacaoContextoList = informacaoContextoRepository.findAll();
        assertThat(informacaoContextoList).hasSize(databaseSizeBeforeUpdate);
        InformacaoContexto testInformacaoContexto = informacaoContextoList.get(informacaoContextoList.size() - 1);
        assertThat(testInformacaoContexto.getNomeOrganizacao()).isEqualTo(UPDATED_NOME_ORGANIZACAO);
        assertThat(testInformacaoContexto.getNumeroColaboradores()).isEqualTo(UPDATED_NUMERO_COLABORADORES);
        assertThat(testInformacaoContexto.getNumeroProjetos()).isEqualTo(UPDATED_NUMERO_PROJETOS);
        assertThat(testInformacaoContexto.getAreaAtuacao()).isEqualTo(UPDATED_AREA_ATUACAO);
        assertThat(testInformacaoContexto.getCertificacao()).isEqualTo(UPDATED_CERTIFICACAO);
    }

    @Test
    @Transactional
    public void updateNonExistingInformacaoContexto() throws Exception {
        int databaseSizeBeforeUpdate = informacaoContextoRepository.findAll().size();

        // Create the InformacaoContexto

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restInformacaoContextoMockMvc.perform(put("/api/informacao-contextos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(informacaoContexto)))
            .andExpect(status().isCreated());

        // Validate the InformacaoContexto in the database
        List<InformacaoContexto> informacaoContextoList = informacaoContextoRepository.findAll();
        assertThat(informacaoContextoList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteInformacaoContexto() throws Exception {
        // Initialize the database
        informacaoContextoRepository.saveAndFlush(informacaoContexto);
        int databaseSizeBeforeDelete = informacaoContextoRepository.findAll().size();

        // Get the informacaoContexto
        restInformacaoContextoMockMvc.perform(delete("/api/informacao-contextos/{id}", informacaoContexto.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<InformacaoContexto> informacaoContextoList = informacaoContextoRepository.findAll();
        assertThat(informacaoContextoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(InformacaoContexto.class);
    }
}
