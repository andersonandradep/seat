package com.seat.web.rest;

import com.seat.SeatApp;

import com.seat.domain.AreaProcesso;
import com.seat.repository.AreaProcessoRepository;
import com.seat.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AreaProcessoResource REST controller.
 *
 * @see AreaProcessoResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SeatApp.class)
public class AreaProcessoResourceIntTest {

    private static final String DEFAULT_NOME = "AAAAAAAAAA";
    private static final String UPDATED_NOME = "BBBBBBBBBB";

    @Autowired
    private AreaProcessoRepository areaProcessoRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restAreaProcessoMockMvc;

    private AreaProcesso areaProcesso;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        AreaProcessoResource areaProcessoResource = new AreaProcessoResource(areaProcessoRepository);
        this.restAreaProcessoMockMvc = MockMvcBuilders.standaloneSetup(areaProcessoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AreaProcesso createEntity(EntityManager em) {
        AreaProcesso areaProcesso = new AreaProcesso();
        areaProcesso.setNome(DEFAULT_NOME);
        return areaProcesso;
    }

    @Before
    public void initTest() {
        areaProcesso = createEntity(em);
    }

    @Test
    @Transactional
    public void createAreaProcesso() throws Exception {
        int databaseSizeBeforeCreate = areaProcessoRepository.findAll().size();

        // Create the AreaProcesso
        restAreaProcessoMockMvc.perform(post("/api/area-processos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(areaProcesso)))
            .andExpect(status().isCreated());

        // Validate the AreaProcesso in the database
        List<AreaProcesso> areaProcessoList = areaProcessoRepository.findAll();
        assertThat(areaProcessoList).hasSize(databaseSizeBeforeCreate + 1);
        AreaProcesso testAreaProcesso = areaProcessoList.get(areaProcessoList.size() - 1);
        assertThat(testAreaProcesso.getNome()).isEqualTo(DEFAULT_NOME);
    }

    @Test
    @Transactional
    public void createAreaProcessoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = areaProcessoRepository.findAll().size();

        // Create the AreaProcesso with an existing ID
        areaProcesso.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAreaProcessoMockMvc.perform(post("/api/area-processos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(areaProcesso)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<AreaProcesso> areaProcessoList = areaProcessoRepository.findAll();
        assertThat(areaProcessoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllAreaProcessos() throws Exception {
        // Initialize the database
        areaProcessoRepository.saveAndFlush(areaProcesso);

        // Get all the areaProcessoList
        restAreaProcessoMockMvc.perform(get("/api/area-processos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(areaProcesso.getId().intValue())))
            .andExpect(jsonPath("$.[*].nome").value(hasItem(DEFAULT_NOME.toString())));
    }

    @Test
    @Transactional
    public void getAreaProcesso() throws Exception {
        // Initialize the database
        areaProcessoRepository.saveAndFlush(areaProcesso);

        // Get the areaProcesso
        restAreaProcessoMockMvc.perform(get("/api/area-processos/{id}", areaProcesso.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(areaProcesso.getId().intValue()))
            .andExpect(jsonPath("$.nome").value(DEFAULT_NOME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingAreaProcesso() throws Exception {
        // Get the areaProcesso
        restAreaProcessoMockMvc.perform(get("/api/area-processos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAreaProcesso() throws Exception {
        // Initialize the database
        areaProcessoRepository.saveAndFlush(areaProcesso);
        int databaseSizeBeforeUpdate = areaProcessoRepository.findAll().size();

        // Update the areaProcesso
        AreaProcesso updatedAreaProcesso = areaProcessoRepository.findOne(areaProcesso.getId());
        updatedAreaProcesso.setNome(UPDATED_NOME);

        restAreaProcessoMockMvc.perform(put("/api/area-processos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedAreaProcesso)))
            .andExpect(status().isOk());

        // Validate the AreaProcesso in the database
        List<AreaProcesso> areaProcessoList = areaProcessoRepository.findAll();
        assertThat(areaProcessoList).hasSize(databaseSizeBeforeUpdate);
        AreaProcesso testAreaProcesso = areaProcessoList.get(areaProcessoList.size() - 1);
        assertThat(testAreaProcesso.getNome()).isEqualTo(UPDATED_NOME);
    }

    @Test
    @Transactional
    public void updateNonExistingAreaProcesso() throws Exception {
        int databaseSizeBeforeUpdate = areaProcessoRepository.findAll().size();

        // Create the AreaProcesso

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restAreaProcessoMockMvc.perform(put("/api/area-processos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(areaProcesso)))
            .andExpect(status().isCreated());

        // Validate the AreaProcesso in the database
        List<AreaProcesso> areaProcessoList = areaProcessoRepository.findAll();
        assertThat(areaProcessoList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteAreaProcesso() throws Exception {
        // Initialize the database
        areaProcessoRepository.saveAndFlush(areaProcesso);
        int databaseSizeBeforeDelete = areaProcessoRepository.findAll().size();

        // Get the areaProcesso
        restAreaProcessoMockMvc.perform(delete("/api/area-processos/{id}", areaProcesso.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<AreaProcesso> areaProcessoList = areaProcessoRepository.findAll();
        assertThat(areaProcessoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AreaProcesso.class);
    }
}
