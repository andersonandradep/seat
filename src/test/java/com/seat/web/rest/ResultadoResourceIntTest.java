package com.seat.web.rest;

import com.seat.SeatApp;

import com.seat.domain.Resultado;
import com.seat.repository.ResultadoRepository;
import com.seat.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ResultadoResource REST controller.
 *
 * @see ResultadoResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SeatApp.class)
public class ResultadoResourceIntTest {

    private static final Integer DEFAULT_NOTA_GERAL = 1;
    private static final Integer UPDATED_NOTA_GERAL = 2;

    private static final Integer DEFAULT_NOTA_PM = 1;
    private static final Integer UPDATED_NOTA_PM = 2;

    private static final Integer DEFAULT_NOTA_SI = 1;
    private static final Integer UPDATED_NOTA_SI = 2;

    @Autowired
    private ResultadoRepository resultadoRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restResultadoMockMvc;

    private Resultado resultado;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ResultadoResource resultadoResource = new ResultadoResource(resultadoRepository);
        this.restResultadoMockMvc = MockMvcBuilders.standaloneSetup(resultadoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Resultado createEntity(EntityManager em) {
        Resultado resultado = new Resultado()
            .notaGeral(DEFAULT_NOTA_GERAL)
            .notaPM(DEFAULT_NOTA_PM)
            .notaSI(DEFAULT_NOTA_SI);
        return resultado;
    }

    @Before
    public void initTest() {
        resultado = createEntity(em);
    }

    @Test
    @Transactional
    public void createResultado() throws Exception {
        int databaseSizeBeforeCreate = resultadoRepository.findAll().size();

        // Create the Resultado
        restResultadoMockMvc.perform(post("/api/resultados")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(resultado)))
            .andExpect(status().isCreated());

        // Validate the Resultado in the database
        List<Resultado> resultadoList = resultadoRepository.findAll();
        assertThat(resultadoList).hasSize(databaseSizeBeforeCreate + 1);
        Resultado testResultado = resultadoList.get(resultadoList.size() - 1);
        assertThat(testResultado.getNotaGeral()).isEqualTo(DEFAULT_NOTA_GERAL);
        assertThat(testResultado.getNotaPM()).isEqualTo(DEFAULT_NOTA_PM);
        assertThat(testResultado.getNotaSI()).isEqualTo(DEFAULT_NOTA_SI);
    }

    @Test
    @Transactional
    public void createResultadoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = resultadoRepository.findAll().size();

        // Create the Resultado with an existing ID
        resultado.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restResultadoMockMvc.perform(post("/api/resultados")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(resultado)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<Resultado> resultadoList = resultadoRepository.findAll();
        assertThat(resultadoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllResultados() throws Exception {
        // Initialize the database
        resultadoRepository.saveAndFlush(resultado);

        // Get all the resultadoList
        restResultadoMockMvc.perform(get("/api/resultados?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(resultado.getId().intValue())))
            .andExpect(jsonPath("$.[*].notaGeral").value(hasItem(DEFAULT_NOTA_GERAL)))
            .andExpect(jsonPath("$.[*].notaPM").value(hasItem(DEFAULT_NOTA_PM)))
            .andExpect(jsonPath("$.[*].notaSI").value(hasItem(DEFAULT_NOTA_SI)));
    }

    @Test
    @Transactional
    public void getResultado() throws Exception {
        // Initialize the database
        resultadoRepository.saveAndFlush(resultado);

        // Get the resultado
        restResultadoMockMvc.perform(get("/api/resultados/{id}", resultado.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(resultado.getId().intValue()))
            .andExpect(jsonPath("$.notaGeral").value(DEFAULT_NOTA_GERAL))
            .andExpect(jsonPath("$.notaPM").value(DEFAULT_NOTA_PM))
            .andExpect(jsonPath("$.notaSI").value(DEFAULT_NOTA_SI));
    }

    @Test
    @Transactional
    public void getNonExistingResultado() throws Exception {
        // Get the resultado
        restResultadoMockMvc.perform(get("/api/resultados/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateResultado() throws Exception {
        // Initialize the database
        resultadoRepository.saveAndFlush(resultado);
        int databaseSizeBeforeUpdate = resultadoRepository.findAll().size();

        // Update the resultado
        Resultado updatedResultado = resultadoRepository.findOne(resultado.getId());
        updatedResultado
            .notaGeral(UPDATED_NOTA_GERAL)
            .notaPM(UPDATED_NOTA_PM)
            .notaSI(UPDATED_NOTA_SI);

        restResultadoMockMvc.perform(put("/api/resultados")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedResultado)))
            .andExpect(status().isOk());

        // Validate the Resultado in the database
        List<Resultado> resultadoList = resultadoRepository.findAll();
        assertThat(resultadoList).hasSize(databaseSizeBeforeUpdate);
        Resultado testResultado = resultadoList.get(resultadoList.size() - 1);
        assertThat(testResultado.getNotaGeral()).isEqualTo(UPDATED_NOTA_GERAL);
        assertThat(testResultado.getNotaPM()).isEqualTo(UPDATED_NOTA_PM);
        assertThat(testResultado.getNotaSI()).isEqualTo(UPDATED_NOTA_SI);
    }

    @Test
    @Transactional
    public void updateNonExistingResultado() throws Exception {
        int databaseSizeBeforeUpdate = resultadoRepository.findAll().size();

        // Create the Resultado

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restResultadoMockMvc.perform(put("/api/resultados")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(resultado)))
            .andExpect(status().isCreated());

        // Validate the Resultado in the database
        List<Resultado> resultadoList = resultadoRepository.findAll();
        assertThat(resultadoList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteResultado() throws Exception {
        // Initialize the database
        resultadoRepository.saveAndFlush(resultado);
        int databaseSizeBeforeDelete = resultadoRepository.findAll().size();

        // Get the resultado
        restResultadoMockMvc.perform(delete("/api/resultados/{id}", resultado.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Resultado> resultadoList = resultadoRepository.findAll();
        assertThat(resultadoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Resultado.class);
    }
}
