package com.seat.web.rest;

import com.seat.SeatApp;

import com.seat.domain.Pergunta;
import com.seat.repository.PerguntaRepository;
import com.seat.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PerguntaResource REST controller.
 *
 * @see PerguntaResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SeatApp.class)
public class PerguntaResourceIntTest {

    private static final String DEFAULT_NOME = "AAAAAAAAAA";
    private static final String UPDATED_NOME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRICAO = "AAAAAAAAAA";
    private static final String UPDATED_DESCRICAO = "BBBBBBBBBB";

    private static final String DEFAULT_DICA = "AAAAAAAAAA";
    private static final String UPDATED_DICA = "BBBBBBBBBB";

    private static final String DEFAULT_LINK = "AAAAAAAAAA";
    private static final String UPDATED_LINK = "BBBBBBBBBB";

    @Autowired
    private PerguntaRepository perguntaRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPerguntaMockMvc;

    private Pergunta pergunta;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PerguntaResource perguntaResource = new PerguntaResource(perguntaRepository);
        this.restPerguntaMockMvc = MockMvcBuilders.standaloneSetup(perguntaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Pergunta createEntity(EntityManager em) {
        Pergunta pergunta = new Pergunta();
        pergunta.setNome(DEFAULT_NOME);
        pergunta.setDescricao(DEFAULT_DESCRICAO);
        pergunta.setDica(DEFAULT_DICA);
        pergunta.setLink(DEFAULT_LINK);
        return pergunta;
    }

    @Before
    public void initTest() {
        pergunta = createEntity(em);
    }

    @Test
    @Transactional
    public void createPergunta() throws Exception {
        int databaseSizeBeforeCreate = perguntaRepository.findAll().size();

        // Create the Pergunta
        restPerguntaMockMvc.perform(post("/api/perguntas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pergunta)))
            .andExpect(status().isCreated());

        // Validate the Pergunta in the database
        List<Pergunta> perguntaList = perguntaRepository.findAll();
        assertThat(perguntaList).hasSize(databaseSizeBeforeCreate + 1);
        Pergunta testPergunta = perguntaList.get(perguntaList.size() - 1);
        assertThat(testPergunta.getNome()).isEqualTo(DEFAULT_NOME);
        assertThat(testPergunta.getDescricao()).isEqualTo(DEFAULT_DESCRICAO);
        assertThat(testPergunta.getDica()).isEqualTo(DEFAULT_DICA);
        assertThat(testPergunta.getLink()).isEqualTo(DEFAULT_LINK);
    }

    @Test
    @Transactional
    public void createPerguntaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = perguntaRepository.findAll().size();

        // Create the Pergunta with an existing ID
        pergunta.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPerguntaMockMvc.perform(post("/api/perguntas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pergunta)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<Pergunta> perguntaList = perguntaRepository.findAll();
        assertThat(perguntaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllPerguntas() throws Exception {
        // Initialize the database
        perguntaRepository.saveAndFlush(pergunta);

        // Get all the perguntaList
        restPerguntaMockMvc.perform(get("/api/perguntas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pergunta.getId().intValue())))
            .andExpect(jsonPath("$.[*].nome").value(hasItem(DEFAULT_NOME.toString())))
            .andExpect(jsonPath("$.[*].descricao").value(hasItem(DEFAULT_DESCRICAO.toString())))
            .andExpect(jsonPath("$.[*].dica").value(hasItem(DEFAULT_DICA.toString())))
            .andExpect(jsonPath("$.[*].link").value(hasItem(DEFAULT_LINK.toString())));
    }

    @Test
    @Transactional
    public void getPergunta() throws Exception {
        // Initialize the database
        perguntaRepository.saveAndFlush(pergunta);

        // Get the pergunta
        restPerguntaMockMvc.perform(get("/api/perguntas/{id}", pergunta.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(pergunta.getId().intValue()))
            .andExpect(jsonPath("$.nome").value(DEFAULT_NOME.toString()))
            .andExpect(jsonPath("$.descricao").value(DEFAULT_DESCRICAO.toString()))
            .andExpect(jsonPath("$.dica").value(DEFAULT_DICA.toString()))
            .andExpect(jsonPath("$.link").value(DEFAULT_LINK.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPergunta() throws Exception {
        // Get the pergunta
        restPerguntaMockMvc.perform(get("/api/perguntas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePergunta() throws Exception {
        // Initialize the database
        perguntaRepository.saveAndFlush(pergunta);
        int databaseSizeBeforeUpdate = perguntaRepository.findAll().size();

        // Update the pergunta
        Pergunta updatedPergunta = perguntaRepository.findOne(pergunta.getId());
        updatedPergunta.setNome(UPDATED_NOME);
        updatedPergunta.setDescricao(UPDATED_DESCRICAO);
        updatedPergunta.setDica(UPDATED_DICA);
        updatedPergunta.setLink(UPDATED_LINK);

        restPerguntaMockMvc.perform(put("/api/perguntas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPergunta)))
            .andExpect(status().isOk());

        // Validate the Pergunta in the database
        List<Pergunta> perguntaList = perguntaRepository.findAll();
        assertThat(perguntaList).hasSize(databaseSizeBeforeUpdate);
        Pergunta testPergunta = perguntaList.get(perguntaList.size() - 1);
        assertThat(testPergunta.getNome()).isEqualTo(UPDATED_NOME);
        assertThat(testPergunta.getDescricao()).isEqualTo(UPDATED_DESCRICAO);
        assertThat(testPergunta.getDica()).isEqualTo(UPDATED_DICA);
        assertThat(testPergunta.getLink()).isEqualTo(UPDATED_LINK);
    }

    @Test
    @Transactional
    public void updateNonExistingPergunta() throws Exception {
        int databaseSizeBeforeUpdate = perguntaRepository.findAll().size();

        // Create the Pergunta

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPerguntaMockMvc.perform(put("/api/perguntas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pergunta)))
            .andExpect(status().isCreated());

        // Validate the Pergunta in the database
        List<Pergunta> perguntaList = perguntaRepository.findAll();
        assertThat(perguntaList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deletePergunta() throws Exception {
        // Initialize the database
        perguntaRepository.saveAndFlush(pergunta);
        int databaseSizeBeforeDelete = perguntaRepository.findAll().size();

        // Get the pergunta
        restPerguntaMockMvc.perform(delete("/api/perguntas/{id}", pergunta.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Pergunta> perguntaList = perguntaRepository.findAll();
        assertThat(perguntaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Pergunta.class);
    }
}
