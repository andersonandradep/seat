package com.seat.web.rest;

import com.seat.SeatApp;

import com.seat.domain.PerguntaDetalhada;
import com.seat.repository.PerguntaDetalhadaRepository;
import com.seat.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PerguntaDetalhadaResource REST controller.
 *
 * @see PerguntaDetalhadaResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SeatApp.class)
public class PerguntaDetalhadaResourceIntTest {

    private static final String DEFAULT_DESCRICAO = "AAAAAAAAAA";
    private static final String UPDATED_DESCRICAO = "BBBBBBBBBB";

    private static final String DEFAULT_DICA = "AAAAAAAAAA";
    private static final String UPDATED_DICA = "BBBBBBBBBB";

    private static final String DEFAULT_LINK = "AAAAAAAAAA";
    private static final String UPDATED_LINK = "BBBBBBBBBB";

    @Autowired
    private PerguntaDetalhadaRepository perguntaDetalhadaRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPerguntaDetalhadaMockMvc;

    private PerguntaDetalhada perguntaDetalhada;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PerguntaDetalhadaResource perguntaDetalhadaResource = new PerguntaDetalhadaResource(perguntaDetalhadaRepository);
        this.restPerguntaDetalhadaMockMvc = MockMvcBuilders.standaloneSetup(perguntaDetalhadaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PerguntaDetalhada createEntity(EntityManager em) {
        PerguntaDetalhada perguntaDetalhada = new PerguntaDetalhada()
            .descricao(DEFAULT_DESCRICAO)
            .dica(DEFAULT_DICA)
            .link(DEFAULT_LINK);
        return perguntaDetalhada;
    }

    @Before
    public void initTest() {
        perguntaDetalhada = createEntity(em);
    }

    @Test
    @Transactional
    public void createPerguntaDetalhada() throws Exception {
        int databaseSizeBeforeCreate = perguntaDetalhadaRepository.findAll().size();

        // Create the PerguntaDetalhada
        restPerguntaDetalhadaMockMvc.perform(post("/api/pergunta-detalhadas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(perguntaDetalhada)))
            .andExpect(status().isCreated());

        // Validate the PerguntaDetalhada in the database
        List<PerguntaDetalhada> perguntaDetalhadaList = perguntaDetalhadaRepository.findAll();
        assertThat(perguntaDetalhadaList).hasSize(databaseSizeBeforeCreate + 1);
        PerguntaDetalhada testPerguntaDetalhada = perguntaDetalhadaList.get(perguntaDetalhadaList.size() - 1);
        assertThat(testPerguntaDetalhada.getDescricao()).isEqualTo(DEFAULT_DESCRICAO);
        assertThat(testPerguntaDetalhada.getDica()).isEqualTo(DEFAULT_DICA);
        assertThat(testPerguntaDetalhada.getLink()).isEqualTo(DEFAULT_LINK);
    }

    @Test
    @Transactional
    public void createPerguntaDetalhadaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = perguntaDetalhadaRepository.findAll().size();

        // Create the PerguntaDetalhada with an existing ID
        perguntaDetalhada.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPerguntaDetalhadaMockMvc.perform(post("/api/pergunta-detalhadas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(perguntaDetalhada)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<PerguntaDetalhada> perguntaDetalhadaList = perguntaDetalhadaRepository.findAll();
        assertThat(perguntaDetalhadaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllPerguntaDetalhadas() throws Exception {
        // Initialize the database
        perguntaDetalhadaRepository.saveAndFlush(perguntaDetalhada);

        // Get all the perguntaDetalhadaList
        restPerguntaDetalhadaMockMvc.perform(get("/api/pergunta-detalhadas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(perguntaDetalhada.getId().intValue())))
            .andExpect(jsonPath("$.[*].descricao").value(hasItem(DEFAULT_DESCRICAO.toString())))
            .andExpect(jsonPath("$.[*].dica").value(hasItem(DEFAULT_DICA.toString())))
            .andExpect(jsonPath("$.[*].link").value(hasItem(DEFAULT_LINK.toString())));
    }

    @Test
    @Transactional
    public void getPerguntaDetalhada() throws Exception {
        // Initialize the database
        perguntaDetalhadaRepository.saveAndFlush(perguntaDetalhada);

        // Get the perguntaDetalhada
        restPerguntaDetalhadaMockMvc.perform(get("/api/pergunta-detalhadas/{id}", perguntaDetalhada.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(perguntaDetalhada.getId().intValue()))
            .andExpect(jsonPath("$.descricao").value(DEFAULT_DESCRICAO.toString()))
            .andExpect(jsonPath("$.dica").value(DEFAULT_DICA.toString()))
            .andExpect(jsonPath("$.link").value(DEFAULT_LINK.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPerguntaDetalhada() throws Exception {
        // Get the perguntaDetalhada
        restPerguntaDetalhadaMockMvc.perform(get("/api/pergunta-detalhadas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePerguntaDetalhada() throws Exception {
        // Initialize the database
        perguntaDetalhadaRepository.saveAndFlush(perguntaDetalhada);
        int databaseSizeBeforeUpdate = perguntaDetalhadaRepository.findAll().size();

        // Update the perguntaDetalhada
        PerguntaDetalhada updatedPerguntaDetalhada = perguntaDetalhadaRepository.findOne(perguntaDetalhada.getId());
        updatedPerguntaDetalhada
            .descricao(UPDATED_DESCRICAO)
            .dica(UPDATED_DICA)
            .link(UPDATED_LINK);

        restPerguntaDetalhadaMockMvc.perform(put("/api/pergunta-detalhadas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPerguntaDetalhada)))
            .andExpect(status().isOk());

        // Validate the PerguntaDetalhada in the database
        List<PerguntaDetalhada> perguntaDetalhadaList = perguntaDetalhadaRepository.findAll();
        assertThat(perguntaDetalhadaList).hasSize(databaseSizeBeforeUpdate);
        PerguntaDetalhada testPerguntaDetalhada = perguntaDetalhadaList.get(perguntaDetalhadaList.size() - 1);
        assertThat(testPerguntaDetalhada.getDescricao()).isEqualTo(UPDATED_DESCRICAO);
        assertThat(testPerguntaDetalhada.getDica()).isEqualTo(UPDATED_DICA);
        assertThat(testPerguntaDetalhada.getLink()).isEqualTo(UPDATED_LINK);
    }

    @Test
    @Transactional
    public void updateNonExistingPerguntaDetalhada() throws Exception {
        int databaseSizeBeforeUpdate = perguntaDetalhadaRepository.findAll().size();

        // Create the PerguntaDetalhada

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPerguntaDetalhadaMockMvc.perform(put("/api/pergunta-detalhadas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(perguntaDetalhada)))
            .andExpect(status().isCreated());

        // Validate the PerguntaDetalhada in the database
        List<PerguntaDetalhada> perguntaDetalhadaList = perguntaDetalhadaRepository.findAll();
        assertThat(perguntaDetalhadaList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deletePerguntaDetalhada() throws Exception {
        // Initialize the database
        perguntaDetalhadaRepository.saveAndFlush(perguntaDetalhada);
        int databaseSizeBeforeDelete = perguntaDetalhadaRepository.findAll().size();

        // Get the perguntaDetalhada
        restPerguntaDetalhadaMockMvc.perform(delete("/api/pergunta-detalhadas/{id}", perguntaDetalhada.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<PerguntaDetalhada> perguntaDetalhadaList = perguntaDetalhadaRepository.findAll();
        assertThat(perguntaDetalhadaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PerguntaDetalhada.class);
    }
}
