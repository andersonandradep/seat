'use strict';

describe('Controller Tests', function() {

    describe('PerguntaDetalhada Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockPerguntaDetalhada, MockPergunta;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockPerguntaDetalhada = jasmine.createSpy('MockPerguntaDetalhada');
            MockPergunta = jasmine.createSpy('MockPergunta');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'PerguntaDetalhada': MockPerguntaDetalhada,
                'Pergunta': MockPergunta
            };
            createController = function() {
                $injector.get('$controller')("PerguntaDetalhadaDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'seatApp:perguntaDetalhadaUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
