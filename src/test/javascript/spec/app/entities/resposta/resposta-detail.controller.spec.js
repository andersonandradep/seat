'use strict';

describe('Controller Tests', function() {

    describe('Resposta Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockResposta, MockAvaliacao, MockPerguntaDetalhada;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockResposta = jasmine.createSpy('MockResposta');
            MockAvaliacao = jasmine.createSpy('MockAvaliacao');
            MockPerguntaDetalhada = jasmine.createSpy('MockPerguntaDetalhada');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'Resposta': MockResposta,
                'Avaliacao': MockAvaliacao,
                'PerguntaDetalhada': MockPerguntaDetalhada
            };
            createController = function() {
                $injector.get('$controller')("RespostaDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'seatApp:respostaUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
