package com.seat.repository;

import com.seat.domain.AreaProcesso;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the AreaProcesso entity.
 */
@SuppressWarnings("unused")
public interface AreaProcessoRepository extends JpaRepository<AreaProcesso,Long> {

}
