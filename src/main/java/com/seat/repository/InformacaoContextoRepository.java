package com.seat.repository;

import com.seat.domain.InformacaoContexto;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the InformacaoContexto entity.
 */
@SuppressWarnings("unused")
public interface InformacaoContextoRepository extends JpaRepository<InformacaoContexto,Long> {

    @Query("select informacaoContexto from InformacaoContexto informacaoContexto where informacaoContexto.user.login = ?#{principal.username}")
    List<InformacaoContexto> findByUserIsCurrentUser();

	InformacaoContexto findByUserLogin(String login);

}
