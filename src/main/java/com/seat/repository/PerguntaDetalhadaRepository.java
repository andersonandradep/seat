package com.seat.repository;

import com.seat.domain.PerguntaDetalhada;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the PerguntaDetalhada entity.
 */
@SuppressWarnings("unused")
public interface PerguntaDetalhadaRepository extends JpaRepository<PerguntaDetalhada,Long> {

}
