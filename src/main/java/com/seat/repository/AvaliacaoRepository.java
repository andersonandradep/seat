package com.seat.repository;

import com.seat.domain.Avaliacao;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Avaliacao entity.
 */
@SuppressWarnings("unused")
public interface AvaliacaoRepository extends JpaRepository<Avaliacao,Long> {

    @Query("select avaliacao from Avaliacao avaliacao where avaliacao.user.login = ?#{principal.username} and status = 'C'")
	List<Avaliacao> getAvaliacoesConcluidas();

    @Query("select count(avaliacao) from Avaliacao avaliacao where status = 'C'")
    Integer getTodasAvaliacoesConcluidas();

}
