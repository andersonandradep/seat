package com.seat.repository;

import com.seat.domain.Resultado;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Resultado entity.
 */
@SuppressWarnings("unused")
public interface ResultadoRepository extends JpaRepository<Resultado,Long> {

}
