package com.seat.repository;

import com.seat.domain.Resposta;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Resposta entity.
 */
@SuppressWarnings("unused")
public interface RespostaRepository extends JpaRepository<Resposta,Long> {
	
	@Query ("select r from Resposta r where r.avaliacao.id = ?1")
	List<Resposta> findAllByAvaliacao(Long avaliacao);

}
