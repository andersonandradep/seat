package com.seat.domain;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Pergunta.
 */
@Entity
@Table(name = "pergunta")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Pergunta implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nome")
    private String nome;

    @Column(name = "descricao")
    private String descricao;

    @Column(name = "dica")
    private String dica;

    @Column(name = "jhi_link")
    private String link;

    @ManyToOne
    private AreaProcesso areaProcesso;

    @OneToMany(mappedBy = "pergunta", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<PerguntaDetalhada> perguntaDetalhadas;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getDica() {
        return dica;
    }

    public void setDica(String dica) {
        this.dica = dica;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public AreaProcesso getAreaProcesso() {
        return areaProcesso;
    }

    public void setAreaProcesso(AreaProcesso areaProcesso) {
        this.areaProcesso = areaProcesso;
    }

    public Set<PerguntaDetalhada> getPerguntaDetalhadas() {
        return perguntaDetalhadas;
    }

    public void setPerguntaDetalhadas(Set<PerguntaDetalhada> perguntaDetalhadas) {
        this.perguntaDetalhadas = perguntaDetalhadas;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Pergunta pergunta = (Pergunta) o;
        if (pergunta.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, pergunta.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Pergunta{" +
            "id=" + id +
            ", nome='" + nome + "'" +
            ", descricao='" + descricao + "'" +
            ", dica='" + dica + "'" +
            ", link='" + link + "'" +
            '}';
    }
}
