package com.seat.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A PerguntaDetalhada.
 */
@Entity
@Table(name = "pergunta_detalhada")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class PerguntaDetalhada implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "descricao")
    private String descricao;

    @Column(name = "dica")
    private String dica;

    @Column(name = "jhi_link")
    private String link;

    @ManyToOne
    @JsonIgnore
    private Pergunta pergunta;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public PerguntaDetalhada descricao(String descricao) {
        this.descricao = descricao;
        return this;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getDica() {
        return dica;
    }

    public PerguntaDetalhada dica(String dica) {
        this.dica = dica;
        return this;
    }

    public void setDica(String dica) {
        this.dica = dica;
    }

    public String getLink() {
        return link;
    }

    public PerguntaDetalhada link(String link) {
        this.link = link;
        return this;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Pergunta getPergunta() {
        return pergunta;
    }

    public PerguntaDetalhada pergunta(Pergunta pergunta) {
        this.pergunta = pergunta;
        return this;
    }

    public void setPergunta(Pergunta pergunta) {
        this.pergunta = pergunta;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PerguntaDetalhada perguntaDetalhada = (PerguntaDetalhada) o;
        if (perguntaDetalhada.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, perguntaDetalhada.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "PerguntaDetalhada{" +
            "id=" + id +
            ", descricao='" + descricao + "'" +
            ", dica='" + dica + "'" +
            ", link='" + link + "'" +
            '}';
    }
}
