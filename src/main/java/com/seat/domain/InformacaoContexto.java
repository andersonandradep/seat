package com.seat.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A InformacaoContexto.
 */
@Entity
@Table(name = "informacao_contexto")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class InformacaoContexto implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nome_organizacao")
    private String nomeOrganizacao;

    @Column(name = "numero_colaboradores")
    private Integer numeroColaboradores;

    @Column(name = "numero_projetos")
    private Integer numeroProjetos;

    @Column(name = "area_atuacao")
    private String areaAtuacao;

    @Column(name = "certificacao")
    private String certificacao;

    @ManyToOne
    private User user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNomeOrganizacao() {
        return nomeOrganizacao;
    }

    public void setNomeOrganizacao(String nomeOrganizacao) {
        this.nomeOrganizacao = nomeOrganizacao;
    }

    public Integer getNumeroColaboradores() {
        return numeroColaboradores;
    }

    public void setNumeroColaboradores(Integer numeroColaboradores) {
        this.numeroColaboradores = numeroColaboradores;
    }

    public Integer getNumeroProjetos() {
        return numeroProjetos;
    }

    public void setNumeroProjetos(Integer numeroProjetos) {
        this.numeroProjetos = numeroProjetos;
    }

    public String getAreaAtuacao() {
        return areaAtuacao;
    }

    public void setAreaAtuacao(String areaAtuacao) {
        this.areaAtuacao = areaAtuacao;
    }

    public String getCertificacao() {
        return certificacao;
    }

    public void setCertificacao(String certificacao) {
        this.certificacao = certificacao;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        InformacaoContexto informacaoContexto = (InformacaoContexto) o;
        if (informacaoContexto.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, informacaoContexto.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "InformacaoContexto{" +
            "id=" + id +
            ", nomeOrganizacao='" + nomeOrganizacao + "'" +
            ", numeroColaboradores='" + numeroColaboradores + "'" +
            ", numeroProjetos='" + numeroProjetos + "'" +
            ", areaAtuacao='" + areaAtuacao + "'" +
            ", certificacao='" + certificacao + "'" +
            '}';
    }
}
