package com.seat.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A AreaProcesso.
 */
@Entity
@Table(name = "area_processo")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class AreaProcesso implements Serializable {

    private static final long serialVersionUID = 1L;
    
    public static final int PM = 1;
    public static final int SI = 2;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nome")
    private String nome;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AreaProcesso areaProcesso = (AreaProcesso) o;
        if (areaProcesso.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, areaProcesso.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "AreaProcesso{" +
            "id=" + id +
            ", nome='" + nome + "'" +
            '}';
    }
}
