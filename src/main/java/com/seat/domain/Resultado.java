package com.seat.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Resultado.
 */
@Entity
@Table(name = "resultado")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Resultado implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nota_geral")
    private Integer notaGeral;

    @Column(name = "nota_pm")
    private Integer notaPM;

    @Column(name = "nota_si")
    private Integer notaSI;

    @OneToOne(mappedBy = "resultado")
    @JsonIgnore
    private Avaliacao avaliacao;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNotaGeral() {
        return notaGeral;
    }

    public Resultado notaGeral(Integer notaGeral) {
        this.notaGeral = notaGeral;
        return this;
    }

    public void setNotaGeral(Integer notaGeral) {
        this.notaGeral = notaGeral;
    }

    public Integer getNotaPM() {
        return notaPM;
    }

    public Resultado notaPM(Integer notaPM) {
        this.notaPM = notaPM;
        return this;
    }

    public void setNotaPM(Integer notaPM) {
        this.notaPM = notaPM;
    }

    public Integer getNotaSI() {
        return notaSI;
    }

    public Resultado notaSI(Integer notaSI) {
        this.notaSI = notaSI;
        return this;
    }

    public void setNotaSI(Integer notaSI) {
        this.notaSI = notaSI;
    }

    public Avaliacao getAvaliacao() {
        return avaliacao;
    }

    public Resultado avaliacao(Avaliacao avaliacao) {
        this.avaliacao = avaliacao;
        return this;
    }

    public void setAvaliacao(Avaliacao avaliacao) {
        this.avaliacao = avaliacao;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Resultado resultado = (Resultado) o;
        if (resultado.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, resultado.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Resultado{" +
            "id=" + id +
            ", notaGeral='" + notaGeral + "'" +
            ", notaPM='" + notaPM + "'" +
            ", notaSI='" + notaSI + "'" +
            '}';
    }
}
