package com.seat.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Resposta.
 */
@Entity
@Table(name = "resposta")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Resposta implements Serializable {

    private static final long serialVersionUID = 1L;
    
	private static final String SIM = "S";
	private static final String NAO = "N";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "resposta")
    private String resposta;

    @ManyToOne
    private Avaliacao avaliacao;

    @ManyToOne
    private PerguntaDetalhada perguntaDetalhada;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getResposta() {
        return resposta;
    }

    public void setResposta(String resposta) {
        this.resposta = resposta;
    }

    public Avaliacao getAvaliacao() {
        return avaliacao;
    }

    public void setAvaliacao(Avaliacao avaliacao) {
        this.avaliacao = avaliacao;
    }

    public PerguntaDetalhada getPerguntaDetalhada() {
        return perguntaDetalhada;
    }

    public void setPerguntaDetalhada(PerguntaDetalhada perguntaDetalhada) {
        this.perguntaDetalhada = perguntaDetalhada;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Resposta resposta = (Resposta) o;
        if (resposta.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, resposta.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Resposta{" +
            "id=" + id +
            ", resposta='" + resposta + "'" +
            '}';
    }
    
    public boolean isSim() {
    	return SIM.equals(getResposta());
    }
    
    public boolean isNao() {
    	return NAO.equals(getResposta());
    }
}
