package com.seat.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.seat.domain.AreaProcesso;

import com.seat.repository.AreaProcessoRepository;
import com.seat.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing AreaProcesso.
 */
@RestController
@RequestMapping("/api")
public class AreaProcessoResource {

    private final Logger log = LoggerFactory.getLogger(AreaProcessoResource.class);

    private static final String ENTITY_NAME = "areaProcesso";
        
    private final AreaProcessoRepository areaProcessoRepository;

    public AreaProcessoResource(AreaProcessoRepository areaProcessoRepository) {
        this.areaProcessoRepository = areaProcessoRepository;
    }

    /**
     * POST  /area-processos : Create a new areaProcesso.
     *
     * @param areaProcesso the areaProcesso to create
     * @return the ResponseEntity with status 201 (Created) and with body the new areaProcesso, or with status 400 (Bad Request) if the areaProcesso has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/area-processos")
    @Timed
    public ResponseEntity<AreaProcesso> createAreaProcesso(@RequestBody AreaProcesso areaProcesso) throws URISyntaxException {
        log.debug("REST request to save AreaProcesso : {}", areaProcesso);
        if (areaProcesso.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new areaProcesso cannot already have an ID")).body(null);
        }
        AreaProcesso result = areaProcessoRepository.save(areaProcesso);
        return ResponseEntity.created(new URI("/api/area-processos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /area-processos : Updates an existing areaProcesso.
     *
     * @param areaProcesso the areaProcesso to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated areaProcesso,
     * or with status 400 (Bad Request) if the areaProcesso is not valid,
     * or with status 500 (Internal Server Error) if the areaProcesso couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/area-processos")
    @Timed
    public ResponseEntity<AreaProcesso> updateAreaProcesso(@RequestBody AreaProcesso areaProcesso) throws URISyntaxException {
        log.debug("REST request to update AreaProcesso : {}", areaProcesso);
        if (areaProcesso.getId() == null) {
            return createAreaProcesso(areaProcesso);
        }
        AreaProcesso result = areaProcessoRepository.save(areaProcesso);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, areaProcesso.getId().toString()))
            .body(result);
    }

    /**
     * GET  /area-processos : get all the areaProcessos.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of areaProcessos in body
     */
    @GetMapping("/area-processos")
    @Timed
    public List<AreaProcesso> getAllAreaProcessos() {
        log.debug("REST request to get all AreaProcessos");
        List<AreaProcesso> areaProcessos = areaProcessoRepository.findAll();
        return areaProcessos;
    }

    /**
     * GET  /area-processos/:id : get the "id" areaProcesso.
     *
     * @param id the id of the areaProcesso to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the areaProcesso, or with status 404 (Not Found)
     */
    @GetMapping("/area-processos/{id}")
    @Timed
    public ResponseEntity<AreaProcesso> getAreaProcesso(@PathVariable Long id) {
        log.debug("REST request to get AreaProcesso : {}", id);
        AreaProcesso areaProcesso = areaProcessoRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(areaProcesso));
    }

    /**
     * DELETE  /area-processos/:id : delete the "id" areaProcesso.
     *
     * @param id the id of the areaProcesso to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/area-processos/{id}")
    @Timed
    public ResponseEntity<Void> deleteAreaProcesso(@PathVariable Long id) {
        log.debug("REST request to delete AreaProcesso : {}", id);
        areaProcessoRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

}
