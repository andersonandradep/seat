/**
 * View Models used by Spring MVC REST controllers.
 */
package com.seat.web.rest.vm;
