package com.seat.web.rest.vm;

public class AvaliacaoResposta {

	private int pergunta;
	private String avaliacao;
	
	public AvaliacaoResposta() {
		
	}

	public int getPergunta() {
		return pergunta;
	}

	public void setPergunta(int pergunta) {
		this.pergunta = pergunta;
	}

	public String getAvaliacao() {
		return avaliacao;
	}

	public void setAvaliacao(String avaliacao) {
		this.avaliacao = avaliacao;
	}

}
