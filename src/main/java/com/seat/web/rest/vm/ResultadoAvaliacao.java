package com.seat.web.rest.vm;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class ResultadoAvaliacao implements Serializable {

	private int notaGeral;
	private String avaliacaoGeral;

	private int notaPM;
	private List<String> avaliacaoPM;

	private int notaSI;
	private List<String> avaliacaoSI;

	private List<AvaliacaoResposta> avaliacoes;
	
	private Map<String, String> avaliacoesPM;
	private Map<String, String> avaliacoesSI;


	public ResultadoAvaliacao() {
	}

	public ResultadoAvaliacao(int notaGeral, String avaliacaoGeral, List<String> avaliacaoPM, List<String> avaliacaoSI, List<AvaliacaoResposta> avaliacoes, Map<String, String> avaliacoesPM, Map<String, String> avaliacoesSI) {
		this.notaGeral = notaGeral;
		this.avaliacaoGeral = avaliacaoGeral;
		this.avaliacaoPM = avaliacaoPM;
		this.avaliacaoSI = avaliacaoSI;
		this.avaliacoes = avaliacoes;
		this.avaliacoesPM = avaliacoesPM;
		this.avaliacoesSI = avaliacoesSI;
	}

	public int getNotaGeral() {
		return notaGeral;
	}

	public void setNotaGeral(int notaGeral) {
		this.notaGeral = notaGeral;
	}

	public String getAvaliacaoGeral() {
		return avaliacaoGeral;
	}

    public int getNotaPM() {
        return notaPM;
    }

    public void setNotaPM(int notaPM) {
        this.notaPM = notaPM;
    }

    public void setAvaliacaoGeral(String avaliacaoGeral) {
		this.avaliacaoGeral = avaliacaoGeral;
	}

    public int getNotaSI() {
        return notaSI;
    }

    public void setNotaSI(int notaSI) {
        this.notaSI = notaSI;
    }

    public List<String> getAvaliacaoPM() {
        return avaliacaoPM;
    }

    public void setAvaliacaoPM(List<String> avaliacaoPM) {
        this.avaliacaoPM = avaliacaoPM;
    }

    public List<String> getAvaliacaoSI() {
        return avaliacaoSI;
    }

    public void setAvaliacaoSI(List<String> avaliacaoSI) {
        this.avaliacaoSI = avaliacaoSI;
    }
    
    public Map<String, String> getAvaliacoesPM() {
		return avaliacoesPM;
	}

	public void setAvaliacoesPM(Map<String, String> avaliacoesPM) {
		this.avaliacoesPM = avaliacoesPM;
	}

	public Map<String, String> getAvaliacoesSI() {
		return avaliacoesSI;
	}

	public void setAvaliacoesSI(Map<String, String> avaliacoesSI) {
		this.avaliacoesSI = avaliacoesSI;
	}

	@Override
    public String toString() {
        return "ResultadoAvaliacao{" +
            "notaGeral=" + notaGeral +
            ", avaliacaoGeral='" + avaliacaoGeral + '\'' +
            ", avaliacaoPM='" + avaliacaoPM + '\'' +
            ", avaliacaoSI='" + avaliacaoSI + '\'' +
            ", avaliacoesPM='" + avaliacoesPM + '\'' +
            ", avaliacoesSI='" + avaliacoesSI + '\'' +
            ", avaliacoes=" + avaliacoes +
            '}';
    }

}
