package com.seat.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.seat.domain.Resultado;

import com.seat.repository.ResultadoRepository;
import com.seat.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * REST controller for managing Resultado.
 */
@RestController
@RequestMapping("/api")
public class ResultadoResource {

    private final Logger log = LoggerFactory.getLogger(ResultadoResource.class);

    private static final String ENTITY_NAME = "resultado";
        
    private final ResultadoRepository resultadoRepository;

    public ResultadoResource(ResultadoRepository resultadoRepository) {
        this.resultadoRepository = resultadoRepository;
    }

    /**
     * POST  /resultados : Create a new resultado.
     *
     * @param resultado the resultado to create
     * @return the ResponseEntity with status 201 (Created) and with body the new resultado, or with status 400 (Bad Request) if the resultado has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/resultados")
    @Timed
    public ResponseEntity<Resultado> createResultado(@RequestBody Resultado resultado) throws URISyntaxException {
        log.debug("REST request to save Resultado : {}", resultado);
        if (resultado.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new resultado cannot already have an ID")).body(null);
        }
        Resultado result = resultadoRepository.save(resultado);
        return ResponseEntity.created(new URI("/api/resultados/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /resultados : Updates an existing resultado.
     *
     * @param resultado the resultado to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated resultado,
     * or with status 400 (Bad Request) if the resultado is not valid,
     * or with status 500 (Internal Server Error) if the resultado couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/resultados")
    @Timed
    public ResponseEntity<Resultado> updateResultado(@RequestBody Resultado resultado) throws URISyntaxException {
        log.debug("REST request to update Resultado : {}", resultado);
        if (resultado.getId() == null) {
            return createResultado(resultado);
        }
        Resultado result = resultadoRepository.save(resultado);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, resultado.getId().toString()))
            .body(result);
    }

    /**
     * GET  /resultados : get all the resultados.
     *
     * @param filter the filter of the request
     * @return the ResponseEntity with status 200 (OK) and the list of resultados in body
     */
    @GetMapping("/resultados")
    @Timed
    public List<Resultado> getAllResultados(@RequestParam(required = false) String filter) {
        if ("avaliacao-is-null".equals(filter)) {
            log.debug("REST request to get all Resultados where avaliacao is null");
            return StreamSupport
                .stream(resultadoRepository.findAll().spliterator(), false)
                .filter(resultado -> resultado.getAvaliacao() == null)
                .collect(Collectors.toList());
        }
        log.debug("REST request to get all Resultados");
        List<Resultado> resultados = resultadoRepository.findAll();
        return resultados;
    }

    /**
     * GET  /resultados/:id : get the "id" resultado.
     *
     * @param id the id of the resultado to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the resultado, or with status 404 (Not Found)
     */
    @GetMapping("/resultados/{id}")
    @Timed
    public ResponseEntity<Resultado> getResultado(@PathVariable Long id) {
        log.debug("REST request to get Resultado : {}", id);
        Resultado resultado = resultadoRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(resultado));
    }

    /**
     * DELETE  /resultados/:id : delete the "id" resultado.
     *
     * @param id the id of the resultado to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/resultados/{id}")
    @Timed
    public ResponseEntity<Void> deleteResultado(@PathVariable Long id) {
        log.debug("REST request to delete Resultado : {}", id);
        resultadoRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

}
