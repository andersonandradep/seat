package com.seat.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.seat.domain.PerguntaDetalhada;

import com.seat.repository.PerguntaDetalhadaRepository;
import com.seat.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing PerguntaDetalhada.
 */
@RestController
@RequestMapping("/api")
public class PerguntaDetalhadaResource {

    private final Logger log = LoggerFactory.getLogger(PerguntaDetalhadaResource.class);

    private static final String ENTITY_NAME = "perguntaDetalhada";
        
    private final PerguntaDetalhadaRepository perguntaDetalhadaRepository;

    public PerguntaDetalhadaResource(PerguntaDetalhadaRepository perguntaDetalhadaRepository) {
        this.perguntaDetalhadaRepository = perguntaDetalhadaRepository;
    }

    /**
     * POST  /pergunta-detalhadas : Create a new perguntaDetalhada.
     *
     * @param perguntaDetalhada the perguntaDetalhada to create
     * @return the ResponseEntity with status 201 (Created) and with body the new perguntaDetalhada, or with status 400 (Bad Request) if the perguntaDetalhada has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/pergunta-detalhadas")
    @Timed
    public ResponseEntity<PerguntaDetalhada> createPerguntaDetalhada(@RequestBody PerguntaDetalhada perguntaDetalhada) throws URISyntaxException {
        log.debug("REST request to save PerguntaDetalhada : {}", perguntaDetalhada);
        if (perguntaDetalhada.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new perguntaDetalhada cannot already have an ID")).body(null);
        }
        PerguntaDetalhada result = perguntaDetalhadaRepository.save(perguntaDetalhada);
        return ResponseEntity.created(new URI("/api/pergunta-detalhadas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /pergunta-detalhadas : Updates an existing perguntaDetalhada.
     *
     * @param perguntaDetalhada the perguntaDetalhada to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated perguntaDetalhada,
     * or with status 400 (Bad Request) if the perguntaDetalhada is not valid,
     * or with status 500 (Internal Server Error) if the perguntaDetalhada couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/pergunta-detalhadas")
    @Timed
    public ResponseEntity<PerguntaDetalhada> updatePerguntaDetalhada(@RequestBody PerguntaDetalhada perguntaDetalhada) throws URISyntaxException {
        log.debug("REST request to update PerguntaDetalhada : {}", perguntaDetalhada);
        if (perguntaDetalhada.getId() == null) {
            return createPerguntaDetalhada(perguntaDetalhada);
        }
        PerguntaDetalhada result = perguntaDetalhadaRepository.save(perguntaDetalhada);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, perguntaDetalhada.getId().toString()))
            .body(result);
    }

    /**
     * GET  /pergunta-detalhadas : get all the perguntaDetalhadas.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of perguntaDetalhadas in body
     */
    @GetMapping("/pergunta-detalhadas")
    @Timed
    public List<PerguntaDetalhada> getAllPerguntaDetalhadas() {
        log.debug("REST request to get all PerguntaDetalhadas");
        List<PerguntaDetalhada> perguntaDetalhadas = perguntaDetalhadaRepository.findAll();
        return perguntaDetalhadas;
    }

    /**
     * GET  /pergunta-detalhadas/:id : get the "id" perguntaDetalhada.
     *
     * @param id the id of the perguntaDetalhada to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the perguntaDetalhada, or with status 404 (Not Found)
     */
    @GetMapping("/pergunta-detalhadas/{id}")
    @Timed
    public ResponseEntity<PerguntaDetalhada> getPerguntaDetalhada(@PathVariable Long id) {
        log.debug("REST request to get PerguntaDetalhada : {}", id);
        PerguntaDetalhada perguntaDetalhada = perguntaDetalhadaRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(perguntaDetalhada));
    }

    /**
     * DELETE  /pergunta-detalhadas/:id : delete the "id" perguntaDetalhada.
     *
     * @param id the id of the perguntaDetalhada to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/pergunta-detalhadas/{id}")
    @Timed
    public ResponseEntity<Void> deletePerguntaDetalhada(@PathVariable Long id) {
        log.debug("REST request to delete PerguntaDetalhada : {}", id);
        perguntaDetalhadaRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

}
