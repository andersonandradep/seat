package com.seat.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import com.seat.domain.Resultado;
import com.seat.repository.ResultadoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.seat.domain.Avaliacao;
import com.seat.repository.AvaliacaoRepository;
import com.seat.repository.UserRepository;
import com.seat.security.SecurityUtils;
import com.seat.service.ResultadoAvaliacaoService;
import com.seat.web.rest.util.HeaderUtil;
import com.seat.web.rest.vm.ResultadoAvaliacao;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing Avaliacao.
 */
@RestController
@RequestMapping("/api")
public class AvaliacaoResource {

    private final Logger log = LoggerFactory.getLogger(AvaliacaoResource.class);

    private static final String ENTITY_NAME = "avaliacao";

    private final AvaliacaoRepository avaliacaoRepository;

    private final ResultadoRepository resultadoRepository;

    private final UserRepository userRepository;

    private final ResultadoAvaliacaoService resultadoAvaliacaoService;

    public AvaliacaoResource(AvaliacaoRepository avaliacaoRepository, UserRepository userRepository, ResultadoAvaliacaoService resultadoAvaliacaoService, ResultadoRepository resultadoRepository) {
        this.avaliacaoRepository = avaliacaoRepository;
        this.userRepository = userRepository;
        this.resultadoAvaliacaoService = resultadoAvaliacaoService;
        this.resultadoRepository = resultadoRepository;
    }

    /**
     * POST  /avaliacaos : Create a new avaliacao.
     *
     * @param avaliacao the avaliacao to create
     * @return the ResponseEntity with status 201 (Created) and with body the new avaliacao, or with status 400 (Bad Request) if the avaliacao has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/avaliacaos")
    @Timed
    public ResponseEntity<Avaliacao> createAvaliacao(@RequestBody Avaliacao avaliacao) throws URISyntaxException {
        log.debug("REST request to save Avaliacao : {}", avaliacao);
        if (avaliacao.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new avaliacao cannot already have an ID")).body(null);
        }
        if (avaliacao.getUser() == null) {
        	avaliacao.setUser(userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin()).get());
        }
        Avaliacao result = avaliacaoRepository.save(avaliacao);
        return ResponseEntity.created(new URI("/api/avaliacaos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /avaliacaos : Updates an existing avaliacao.
     *
     * @param avaliacao the avaliacao to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated avaliacao,
     * or with status 400 (Bad Request) if the avaliacao is not valid,
     * or with status 500 (Internal Server Error) if the avaliacao couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/avaliacaos")
    @Timed
    public ResponseEntity<Avaliacao> updateAvaliacao(@RequestBody Avaliacao avaliacao) throws URISyntaxException {
        log.debug("REST request to update Avaliacao : {}", avaliacao);
        if (avaliacao.getId() == null) {
            return createAvaliacao(avaliacao);
        }
        Avaliacao result = avaliacaoRepository.save(avaliacao);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, avaliacao.getId().toString()))
            .body(result);
    }

    /**
     * GET  /avaliacaos : get all the avaliacaos.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of avaliacaos in body
     */
    @GetMapping("/avaliacaos")
    @Timed
    public List<Avaliacao> getAllAvaliacaos() {
        log.debug("REST request to get all Avaliacaos");
        List<Avaliacao> avaliacaos = avaliacaoRepository.findAll();
        return avaliacaos;
    }

    /**
     * GET  /avaliacaos/:id : get the "id" avaliacao.
     *
     * @param id the id of the avaliacao to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the avaliacao, or with status 404 (Not Found)
     */
    @GetMapping("/avaliacaos/{id}")
    @Timed
    public ResponseEntity<Avaliacao> getAvaliacao(@PathVariable Long id) {
        log.debug("REST request to get Avaliacao : {}", id);
        Avaliacao avaliacao = avaliacaoRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(avaliacao));
    }

    /**
     * DELETE  /avaliacaos/:id : delete the "id" avaliacao.
     *
     * @param id the id of the avaliacao to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/avaliacaos/{id}")
    @Timed
    public ResponseEntity<Void> deleteAvaliacao(@PathVariable Long id) {
        log.debug("REST request to delete Avaliacao : {}", id);
        avaliacaoRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

	@GetMapping("/result/{id}")
	@Timed
	public ResponseEntity<ResultadoAvaliacao> getResultado(@PathVariable Long id) {
        ResultadoAvaliacao resultadoAvaliacao = resultadoAvaliacaoService.processaResultado(id);

        Avaliacao avaliacao = avaliacaoRepository.findOne(id);
        if (avaliacao.getResultado() == null) {
            avaliacao.setResultado(salvaResultado(resultadoAvaliacao, id));
            avaliacaoRepository.save(avaliacao);
        }

		return new ResponseEntity<>(resultadoAvaliacao, HttpStatus.OK);
	}

    private Resultado salvaResultado(ResultadoAvaliacao resultadoAvaliacao, Long id) {
        Resultado resultado = new Resultado();
        resultado.setAvaliacao(new Avaliacao(id));

        resultado.setNotaGeral(resultadoAvaliacao.getNotaGeral());
        resultado.setNotaPM(resultadoAvaliacao.getNotaPM());
        resultado.setNotaSI(resultadoAvaliacao.getNotaSI());

        return resultadoRepository.saveAndFlush(resultado);
    }

    @GetMapping("/avaliacoesConcluidas")
	@Timed
	public List<Avaliacao> getAvaliacoesConcluidas() {
		return avaliacaoRepository.getAvaliacoesConcluidas();
	}

    @GetMapping("/todasAvaliacoesConcluidas")
    @Timed
    public Integer getTodasAvaliacoesConcluidas() {
        return avaliacaoRepository.getTodasAvaliacoesConcluidas();
    }

}
