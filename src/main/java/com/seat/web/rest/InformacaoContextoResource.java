package com.seat.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.seat.domain.InformacaoContexto;
import com.seat.repository.InformacaoContextoRepository;
import com.seat.repository.UserRepository;
import com.seat.security.AuthoritiesConstants;
import com.seat.security.SecurityUtils;
import com.seat.web.rest.util.HeaderUtil;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing InformacaoContexto.
 */
@RestController
@RequestMapping("/api")
public class InformacaoContextoResource {

    private final Logger log = LoggerFactory.getLogger(InformacaoContextoResource.class);

    private static final String ENTITY_NAME = "informacaoContexto";
        
    private final InformacaoContextoRepository informacaoContextoRepository;
    
    private final UserRepository userRepository;

    public InformacaoContextoResource(InformacaoContextoRepository informacaoContextoRepository, UserRepository userRepository) {
        this.informacaoContextoRepository = informacaoContextoRepository;
        this.userRepository = userRepository;
    }

    /**
     * POST  /informacao-contextos : Create a new informacaoContexto.
     *
     * @param informacaoContexto the informacaoContexto to create
     * @return the ResponseEntity with status 201 (Created) and with body the new informacaoContexto, or with status 400 (Bad Request) if the informacaoContexto has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/informacao-contextos")
    @Timed
    public ResponseEntity<InformacaoContexto> createInformacaoContexto(@RequestBody InformacaoContexto informacaoContexto) throws URISyntaxException {
        log.debug("REST request to save InformacaoContexto : {}", informacaoContexto);
        if (informacaoContexto.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new informacaoContexto cannot already have an ID")).body(null);
        }
        if (informacaoContexto.getUser() == null) {
        	informacaoContexto.setUser(userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin()).get());
        }
        InformacaoContexto result = informacaoContextoRepository.save(informacaoContexto);
        return ResponseEntity.created(new URI("/api/informacao-contextos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /informacao-contextos : Updates an existing informacaoContexto.
     *
     * @param informacaoContexto the informacaoContexto to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated informacaoContexto,
     * or with status 400 (Bad Request) if the informacaoContexto is not valid,
     * or with status 500 (Internal Server Error) if the informacaoContexto couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/informacao-contextos")
    @Timed
    public ResponseEntity<InformacaoContexto> updateInformacaoContexto(@RequestBody InformacaoContexto informacaoContexto) throws URISyntaxException {
        log.debug("REST request to update InformacaoContexto : {}", informacaoContexto);
        if (informacaoContexto.getId() == null) {
            return createInformacaoContexto(informacaoContexto);
        }
        InformacaoContexto result = informacaoContextoRepository.save(informacaoContexto);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, informacaoContexto.getId().toString()))
            .body(result);
    }

    /**
     * GET  /informacao-contextos : get all the informacaoContextos.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of informacaoContextos in body
     */
    @GetMapping("/informacao-contextos")
    @Timed
    public List<InformacaoContexto> getAllInformacaoContextos() {
        log.debug("REST request to get all InformacaoContextos");
        List<InformacaoContexto> informacaoContextos;
        if (SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN)) {
        	informacaoContextos = informacaoContextoRepository.findAll();
        } else {
        	informacaoContextos = informacaoContextoRepository.findByUserIsCurrentUser();
        }
        return informacaoContextos;
    }
    
    @GetMapping("/informacoes-contexto")
    @Timed
    public InformacaoContexto getInformacoesContexto() {
        return (InformacaoContexto) informacaoContextoRepository.findByUserLogin(SecurityUtils.getCurrentUserLogin());
    }

    /**
     * GET  /informacao-contextos/:id : get the "id" informacaoContexto.
     *
     * @param id the id of the informacaoContexto to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the informacaoContexto, or with status 404 (Not Found)
     */
    @GetMapping("/informacao-contextos/{id}")
    @Timed
    public ResponseEntity<InformacaoContexto> getInformacaoContexto(@PathVariable Long id) {
        log.debug("REST request to get InformacaoContexto : {}", id);
        InformacaoContexto informacaoContexto = informacaoContextoRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(informacaoContexto));
    }

    /**
     * DELETE  /informacao-contextos/:id : delete the "id" informacaoContexto.
     *
     * @param id the id of the informacaoContexto to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/informacao-contextos/{id}")
    @Timed
    public ResponseEntity<Void> deleteInformacaoContexto(@PathVariable Long id) {
        log.debug("REST request to delete InformacaoContexto : {}", id);
        informacaoContextoRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

}
