package com.seat.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.seat.domain.Pergunta;

import com.seat.repository.PerguntaRepository;
import com.seat.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Pergunta.
 */
@RestController
@RequestMapping("/api")
public class PerguntaResource {

    private final Logger log = LoggerFactory.getLogger(PerguntaResource.class);

    private static final String ENTITY_NAME = "pergunta";
        
    private final PerguntaRepository perguntaRepository;

    public PerguntaResource(PerguntaRepository perguntaRepository) {
        this.perguntaRepository = perguntaRepository;
    }

    /**
     * POST  /perguntas : Create a new pergunta.
     *
     * @param pergunta the pergunta to create
     * @return the ResponseEntity with status 201 (Created) and with body the new pergunta, or with status 400 (Bad Request) if the pergunta has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/perguntas")
    @Timed
    public ResponseEntity<Pergunta> createPergunta(@RequestBody Pergunta pergunta) throws URISyntaxException {
        log.debug("REST request to save Pergunta : {}", pergunta);
        if (pergunta.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new pergunta cannot already have an ID")).body(null);
        }
        Pergunta result = perguntaRepository.save(pergunta);
        return ResponseEntity.created(new URI("/api/perguntas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /perguntas : Updates an existing pergunta.
     *
     * @param pergunta the pergunta to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated pergunta,
     * or with status 400 (Bad Request) if the pergunta is not valid,
     * or with status 500 (Internal Server Error) if the pergunta couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/perguntas")
    @Timed
    public ResponseEntity<Pergunta> updatePergunta(@RequestBody Pergunta pergunta) throws URISyntaxException {
        log.debug("REST request to update Pergunta : {}", pergunta);
        if (pergunta.getId() == null) {
            return createPergunta(pergunta);
        }
        Pergunta result = perguntaRepository.save(pergunta);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, pergunta.getId().toString()))
            .body(result);
    }

    /**
     * GET  /perguntas : get all the perguntas.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of perguntas in body
     */
    @GetMapping("/perguntas")
    @Timed
    public List<Pergunta> getAllPerguntas() {
        log.debug("REST request to get all Perguntas");
        List<Pergunta> perguntas = perguntaRepository.findAll();
        return perguntas;
    }

    /**
     * GET  /perguntas/:id : get the "id" pergunta.
     *
     * @param id the id of the pergunta to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the pergunta, or with status 404 (Not Found)
     */
    @GetMapping("/perguntas/{id}")
    @Timed
    public ResponseEntity<Pergunta> getPergunta(@PathVariable Long id) {
        log.debug("REST request to get Pergunta : {}", id);
        Pergunta pergunta = perguntaRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(pergunta));
    }

    /**
     * DELETE  /perguntas/:id : delete the "id" pergunta.
     *
     * @param id the id of the pergunta to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/perguntas/{id}")
    @Timed
    public ResponseEntity<Void> deletePergunta(@PathVariable Long id) {
        log.debug("REST request to delete Pergunta : {}", id);
        perguntaRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

}
