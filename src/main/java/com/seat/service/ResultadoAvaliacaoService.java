package com.seat.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.seat.domain.AreaProcesso;
import com.seat.domain.Resposta;
import com.seat.repository.RespostaRepository;
import com.seat.web.rest.vm.ResultadoAvaliacao;

@Service
@Transactional
public class ResultadoAvaliacaoService {

	private final RespostaRepository respostaRepository;

	private ResultadoAvaliacao resultadoAvaliacao = new ResultadoAvaliacao();

	public ResultadoAvaliacaoService(RespostaRepository respostaRepository) {
		this.respostaRepository = respostaRepository;
	}

	@Transactional(readOnly = true)
	public ResultadoAvaliacao processaResultado(Long avaliacaoID) {
		List<Resposta> respostas = respostaRepository.findAllByAvaliacao(avaliacaoID);

        int numeroRespostas = respostas.size();
        int numeroRespostasPM = 0;
        int numeroRespostasSI = 0;
        
        int totalSim = 0;
        int totalPM = 0;
        int totalSI = 0;

        List<String> avaliacaoPM = new ArrayList<>();
        List<String> avaliacaoSI = new ArrayList<>();
        
        Map<String, String> avaliacoesPM = new HashMap<>();
        Map<String, String> avaliacoesSI = new HashMap<>();

        for (Resposta resposta : respostas) {
        	if (AreaProcesso.PM == resposta.getPerguntaDetalhada().getPergunta().getAreaProcesso().getId()) {
        		numeroRespostasPM++;
        		if (resposta.isSim()) {
        			totalSim++;
        			totalPM++;
        		} else {
        			avaliacaoPM.add(resposta.getPerguntaDetalhada().getDica());
        			
        			avaliacoesPM.put(resposta.getPerguntaDetalhada().getDescricao(), resposta.getPerguntaDetalhada().getDica());
        		}
            } else {
            	numeroRespostasSI++;
            	if (resposta.isSim()) {
            		totalSim++;
            		totalSI++;
            	} else {
            		avaliacaoSI.add(resposta.getPerguntaDetalhada().getDica());
            		
            		avaliacoesSI.put(resposta.getPerguntaDetalhada().getDescricao(), resposta.getPerguntaDetalhada().getDica());
            	}
            }
        }

        resultadoAvaliacao.setNotaGeral(totalSim * 100 / numeroRespostas);
        resultadoAvaliacao.setNotaPM(totalPM * 100 / numeroRespostasPM);
        resultadoAvaliacao.setNotaSI(totalSI * 100 / numeroRespostasSI);

        resultadoAvaliacao.setAvaliacaoPM(avaliacaoPM);
        resultadoAvaliacao.setAvaliacaoSI(avaliacaoSI);

        resultadoAvaliacao.setAvaliacaoGeral(getAvaliacaoGeral());
        
        resultadoAvaliacao.setAvaliacoesPM(avaliacoesPM);
        resultadoAvaliacao.setAvaliacoesSI(avaliacoesSI);

        salvaResultado(avaliacaoID);

		return resultadoAvaliacao;
	}

    private void salvaResultado(Long avaliacaoID) {

    }

    private String getAvaliacaoGeral() {
		String avaliacao = "";
		if (resultadoAvaliacao.getNotaGeral() <= 15) {
			avaliacao = "avaliacao.pessimo";
		}
		if (resultadoAvaliacao.getNotaGeral() > 15 && resultadoAvaliacao.getNotaGeral() <= 50) {
			avaliacao = "avaliacao.ruim";
		}
		if (resultadoAvaliacao.getNotaGeral() > 50 && resultadoAvaliacao.getNotaGeral() <= 85) {
			avaliacao = "avaliacao.bom";
		}
		if (resultadoAvaliacao.getNotaGeral() > 85) {
			avaliacao = "avaliacao.otimo";
		}
		return avaliacao;
	}

}
