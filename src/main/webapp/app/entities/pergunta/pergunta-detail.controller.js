(function() {
    'use strict';

    angular
        .module('seatApp')
        .controller('PerguntaDetailController', PerguntaDetailController);

    PerguntaDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Pergunta', 'AreaProcesso', 'PerguntaDetalhada'];

    function PerguntaDetailController($scope, $rootScope, $stateParams, previousState, entity, Pergunta, AreaProcesso, PerguntaDetalhada) {
        var vm = this;

        vm.pergunta = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('seatApp:perguntaUpdate', function(event, result) {
            vm.pergunta = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
