(function() {
    'use strict';

    angular
        .module('seatApp')
        .controller('PerguntaDialogController', PerguntaDialogController);

    PerguntaDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Pergunta', 'AreaProcesso', 'PerguntaDetalhada'];

    function PerguntaDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Pergunta, AreaProcesso, PerguntaDetalhada) {
        var vm = this;

        vm.pergunta = entity;
        vm.clear = clear;
        vm.save = save;
        vm.areaprocessos = AreaProcesso.query();
        vm.perguntadetalhadas = PerguntaDetalhada.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.pergunta.id !== null) {
                Pergunta.update(vm.pergunta, onSaveSuccess, onSaveError);
            } else {
                Pergunta.save(vm.pergunta, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('seatApp:perguntaUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
