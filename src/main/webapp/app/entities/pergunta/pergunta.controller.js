(function() {
    'use strict';

    angular
        .module('seatApp')
        .controller('PerguntaController', PerguntaController);

    PerguntaController.$inject = ['Pergunta'];

    function PerguntaController(Pergunta) {

        var vm = this;

        vm.perguntas = [];

        loadAll();

        function loadAll() {
            Pergunta.query(function(result) {
                vm.perguntas = result;
                vm.searchQuery = null;
            });
        }
    }
})();
