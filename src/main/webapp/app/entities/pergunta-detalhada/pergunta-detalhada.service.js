(function() {
    'use strict';
    angular
        .module('seatApp')
        .factory('PerguntaDetalhada', PerguntaDetalhada);

    PerguntaDetalhada.$inject = ['$resource'];

    function PerguntaDetalhada ($resource) {
        var resourceUrl =  'api/pergunta-detalhadas/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
