(function() {
    'use strict';

    angular
        .module('seatApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('pergunta-detalhada', {
            parent: 'entity',
            url: '/pergunta-detalhada',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'seatApp.perguntaDetalhada.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/pergunta-detalhada/pergunta-detalhadas.html',
                    controller: 'PerguntaDetalhadaController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('perguntaDetalhada');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('pergunta-detalhada-detail', {
            parent: 'pergunta-detalhada',
            url: '/pergunta-detalhada/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'seatApp.perguntaDetalhada.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/pergunta-detalhada/pergunta-detalhada-detail.html',
                    controller: 'PerguntaDetalhadaDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('perguntaDetalhada');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'PerguntaDetalhada', function($stateParams, PerguntaDetalhada) {
                    return PerguntaDetalhada.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'pergunta-detalhada',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('pergunta-detalhada-detail.edit', {
            parent: 'pergunta-detalhada-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/pergunta-detalhada/pergunta-detalhada-dialog.html',
                    controller: 'PerguntaDetalhadaDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['PerguntaDetalhada', function(PerguntaDetalhada) {
                            return PerguntaDetalhada.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('pergunta-detalhada.new', {
            parent: 'pergunta-detalhada',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/pergunta-detalhada/pergunta-detalhada-dialog.html',
                    controller: 'PerguntaDetalhadaDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                descricao: null,
                                dica: null,
                                link: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('pergunta-detalhada', null, { reload: 'pergunta-detalhada' });
                }, function() {
                    $state.go('pergunta-detalhada');
                });
            }]
        })
        .state('pergunta-detalhada.edit', {
            parent: 'pergunta-detalhada',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/pergunta-detalhada/pergunta-detalhada-dialog.html',
                    controller: 'PerguntaDetalhadaDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['PerguntaDetalhada', function(PerguntaDetalhada) {
                            return PerguntaDetalhada.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('pergunta-detalhada', null, { reload: 'pergunta-detalhada' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('pergunta-detalhada.delete', {
            parent: 'pergunta-detalhada',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/pergunta-detalhada/pergunta-detalhada-delete-dialog.html',
                    controller: 'PerguntaDetalhadaDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['PerguntaDetalhada', function(PerguntaDetalhada) {
                            return PerguntaDetalhada.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('pergunta-detalhada', null, { reload: 'pergunta-detalhada' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
