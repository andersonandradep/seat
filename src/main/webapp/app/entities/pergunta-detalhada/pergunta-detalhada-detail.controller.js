(function() {
    'use strict';

    angular
        .module('seatApp')
        .controller('PerguntaDetalhadaDetailController', PerguntaDetalhadaDetailController);

    PerguntaDetalhadaDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'PerguntaDetalhada', 'Pergunta'];

    function PerguntaDetalhadaDetailController($scope, $rootScope, $stateParams, previousState, entity, PerguntaDetalhada, Pergunta) {
        var vm = this;

        vm.perguntaDetalhada = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('seatApp:perguntaDetalhadaUpdate', function(event, result) {
            vm.perguntaDetalhada = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
