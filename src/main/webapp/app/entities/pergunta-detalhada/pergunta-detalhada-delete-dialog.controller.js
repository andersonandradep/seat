(function() {
    'use strict';

    angular
        .module('seatApp')
        .controller('PerguntaDetalhadaDeleteController',PerguntaDetalhadaDeleteController);

    PerguntaDetalhadaDeleteController.$inject = ['$uibModalInstance', 'entity', 'PerguntaDetalhada'];

    function PerguntaDetalhadaDeleteController($uibModalInstance, entity, PerguntaDetalhada) {
        var vm = this;

        vm.perguntaDetalhada = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            PerguntaDetalhada.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
