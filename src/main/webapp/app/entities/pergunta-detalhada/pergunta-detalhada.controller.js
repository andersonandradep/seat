(function() {
    'use strict';

    angular
        .module('seatApp')
        .controller('PerguntaDetalhadaController', PerguntaDetalhadaController);

    PerguntaDetalhadaController.$inject = ['PerguntaDetalhada'];

    function PerguntaDetalhadaController(PerguntaDetalhada) {

        var vm = this;

        vm.perguntaDetalhadas = [];

        loadAll();

        function loadAll() {
            PerguntaDetalhada.query(function(result) {
                vm.perguntaDetalhadas = result;
                vm.searchQuery = null;
            });
        }
    }
})();
