(function() {
    'use strict';

    angular
        .module('seatApp')
        .controller('PerguntaDetalhadaDialogController', PerguntaDetalhadaDialogController);

    PerguntaDetalhadaDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'PerguntaDetalhada', 'Pergunta'];

    function PerguntaDetalhadaDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, PerguntaDetalhada, Pergunta) {
        var vm = this;

        vm.perguntaDetalhada = entity;
        vm.clear = clear;
        vm.save = save;
        vm.perguntas = Pergunta.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.perguntaDetalhada.id !== null) {
                PerguntaDetalhada.update(vm.perguntaDetalhada, onSaveSuccess, onSaveError);
            } else {
                PerguntaDetalhada.save(vm.perguntaDetalhada, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('seatApp:perguntaDetalhadaUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
