(function() {
    'use strict';

    angular
        .module('seatApp')
        .controller('AvaliacaoDialogController', AvaliacaoDialogController);

    AvaliacaoDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'Avaliacao', 'User', 'Resultado'];

    function AvaliacaoDialogController ($timeout, $scope, $stateParams, $uibModalInstance, $q, entity, Avaliacao, User, Resultado) {
        var vm = this;

        vm.avaliacao = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.users = User.query();
        vm.resultados = Resultado.query({filter: 'avaliacao-is-null'});
        $q.all([vm.avaliacao.$promise, vm.resultados.$promise]).then(function() {
            if (!vm.avaliacao.resultado || !vm.avaliacao.resultado.id) {
                return $q.reject();
            }
            return Resultado.get({id : vm.avaliacao.resultado.id}).$promise;
        }).then(function(resultado) {
            vm.resultados.push(resultado);
        });

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.avaliacao.id !== null) {
                Avaliacao.update(vm.avaliacao, onSaveSuccess, onSaveError);
            } else {
                Avaliacao.save(vm.avaliacao, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('seatApp:avaliacaoUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.data = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
