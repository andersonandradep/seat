(function() {
    'use strict';
    angular
        .module('seatApp')
        .factory('Avaliacao', Avaliacao);

    Avaliacao.$inject = ['$resource', 'DateUtils'];

    function Avaliacao ($resource, DateUtils) {
        var resourceUrl =  'api/avaliacaos/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'getResultado': { method: 'GET', isArray: false, url: 'api/result/:id'},
            'getAvaliacoesConcluidas': { method: 'GET', isArray: true, url: 'api/avaliacoesConcluidas'},
            'getTodasAvaliacoesConcluidas': { method: 'GET', isArray: false, url: 'api/todasAvaliacoesConcluidas'},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.data = DateUtils.convertDateTimeFromServer(data.data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
