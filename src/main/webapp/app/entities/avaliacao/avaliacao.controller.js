(function() {
    'use strict';

    angular
        .module('seatApp')
        .controller('AvaliacaoController', AvaliacaoController);

    AvaliacaoController.$inject = ['Avaliacao'];

    function AvaliacaoController(Avaliacao) {

        var vm = this;

        vm.avaliacaos = [];

        loadAll();

        function loadAll() {
            Avaliacao.query(function(result) {
                vm.avaliacaos = result;
                vm.searchQuery = null;
            });
        }
    }
})();
