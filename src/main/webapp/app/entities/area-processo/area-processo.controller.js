(function() {
    'use strict';

    angular
        .module('seatApp')
        .controller('AreaProcessoController', AreaProcessoController);

    AreaProcessoController.$inject = ['AreaProcesso'];

    function AreaProcessoController(AreaProcesso) {

        var vm = this;

        vm.areaProcessos = [];

        loadAll();

        function loadAll() {
            AreaProcesso.query(function(result) {
                vm.areaProcessos = result;
                vm.searchQuery = null;
            });
        }
    }
})();
