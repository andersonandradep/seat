(function() {
    'use strict';

    angular
        .module('seatApp')
        .controller('AreaProcessoDetailController', AreaProcessoDetailController);

    AreaProcessoDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'AreaProcesso'];

    function AreaProcessoDetailController($scope, $rootScope, $stateParams, previousState, entity, AreaProcesso) {
        var vm = this;

        vm.areaProcesso = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('seatApp:areaProcessoUpdate', function(event, result) {
            vm.areaProcesso = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
