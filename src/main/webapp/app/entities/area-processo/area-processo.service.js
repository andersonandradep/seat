(function() {
    'use strict';
    angular
        .module('seatApp')
        .factory('AreaProcesso', AreaProcesso);

    AreaProcesso.$inject = ['$resource'];

    function AreaProcesso ($resource) {
        var resourceUrl =  'api/area-processos/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
