(function() {
    'use strict';

    angular
        .module('seatApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('area-processo', {
            parent: 'entity',
            url: '/area-processo',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'seatApp.areaProcesso.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/area-processo/area-processos.html',
                    controller: 'AreaProcessoController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('areaProcesso');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('area-processo-detail', {
            parent: 'area-processo',
            url: '/area-processo/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'seatApp.areaProcesso.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/area-processo/area-processo-detail.html',
                    controller: 'AreaProcessoDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('areaProcesso');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'AreaProcesso', function($stateParams, AreaProcesso) {
                    return AreaProcesso.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'area-processo',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('area-processo-detail.edit', {
            parent: 'area-processo-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/area-processo/area-processo-dialog.html',
                    controller: 'AreaProcessoDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['AreaProcesso', function(AreaProcesso) {
                            return AreaProcesso.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('area-processo.new', {
            parent: 'area-processo',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/area-processo/area-processo-dialog.html',
                    controller: 'AreaProcessoDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                nome: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('area-processo', null, { reload: 'area-processo' });
                }, function() {
                    $state.go('area-processo');
                });
            }]
        })
        .state('area-processo.edit', {
            parent: 'area-processo',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/area-processo/area-processo-dialog.html',
                    controller: 'AreaProcessoDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['AreaProcesso', function(AreaProcesso) {
                            return AreaProcesso.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('area-processo', null, { reload: 'area-processo' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('area-processo.delete', {
            parent: 'area-processo',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/area-processo/area-processo-delete-dialog.html',
                    controller: 'AreaProcessoDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['AreaProcesso', function(AreaProcesso) {
                            return AreaProcesso.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('area-processo', null, { reload: 'area-processo' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
