(function() {
    'use strict';

    angular
        .module('seatApp')
        .controller('AreaProcessoDialogController', AreaProcessoDialogController);

    AreaProcessoDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'AreaProcesso'];

    function AreaProcessoDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, AreaProcesso) {
        var vm = this;

        vm.areaProcesso = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.areaProcesso.id !== null) {
                AreaProcesso.update(vm.areaProcesso, onSaveSuccess, onSaveError);
            } else {
                AreaProcesso.save(vm.areaProcesso, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('seatApp:areaProcessoUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
