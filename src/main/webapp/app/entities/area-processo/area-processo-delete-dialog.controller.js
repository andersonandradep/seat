(function() {
    'use strict';

    angular
        .module('seatApp')
        .controller('AreaProcessoDeleteController',AreaProcessoDeleteController);

    AreaProcessoDeleteController.$inject = ['$uibModalInstance', 'entity', 'AreaProcesso'];

    function AreaProcessoDeleteController($uibModalInstance, entity, AreaProcesso) {
        var vm = this;

        vm.areaProcesso = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            AreaProcesso.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
