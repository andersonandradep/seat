(function() {
    'use strict';

    angular
        .module('seatApp')
        .controller('RespostaDetailController', RespostaDetailController);

    RespostaDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Resposta', 'Avaliacao', 'PerguntaDetalhada'];

    function RespostaDetailController($scope, $rootScope, $stateParams, previousState, entity, Resposta, Avaliacao, PerguntaDetalhada) {
        var vm = this;

        vm.resposta = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('seatApp:respostaUpdate', function(event, result) {
            vm.resposta = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
