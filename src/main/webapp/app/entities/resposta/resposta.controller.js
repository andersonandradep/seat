(function() {
    'use strict';

    angular
        .module('seatApp')
        .controller('RespostaController', RespostaController);

    RespostaController.$inject = ['Resposta'];

    function RespostaController(Resposta) {

        var vm = this;

        vm.respostas = [];

        loadAll();

        function loadAll() {
            Resposta.query(function(result) {
                vm.respostas = result;
                vm.searchQuery = null;
            });
        }
    }
})();
