(function() {
    'use strict';

    angular
        .module('seatApp')
        .controller('RespostaDialogController', RespostaDialogController);

    RespostaDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Resposta', 'Avaliacao', 'PerguntaDetalhada'];

    function RespostaDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Resposta, Avaliacao, PerguntaDetalhada) {
        var vm = this;

        vm.resposta = entity;
        vm.clear = clear;
        vm.save = save;
        vm.avaliacaos = Avaliacao.query();
        vm.perguntadetalhadas = PerguntaDetalhada.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.resposta.id !== null) {
                Resposta.update(vm.resposta, onSaveSuccess, onSaveError);
            } else {
                Resposta.save(vm.resposta, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('seatApp:respostaUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
