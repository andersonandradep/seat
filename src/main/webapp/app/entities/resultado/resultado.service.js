(function() {
    'use strict';
    angular
        .module('seatApp')
        .factory('Resultado', Resultado);

    Resultado.$inject = ['$resource'];

    function Resultado ($resource) {
        var resourceUrl =  'api/resultados/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
