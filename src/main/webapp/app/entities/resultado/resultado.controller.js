(function() {
    'use strict';

    angular
        .module('seatApp')
        .controller('ResultadoController', ResultadoController);

    ResultadoController.$inject = ['Resultado'];

    function ResultadoController(Resultado) {

        var vm = this;

        vm.resultados = [];

        loadAll();

        function loadAll() {
            Resultado.query(function(result) {
                vm.resultados = result;
                vm.searchQuery = null;
            });
        }
    }
})();
