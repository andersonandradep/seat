(function() {
    'use strict';

    angular
        .module('seatApp')
        .controller('ResultadoDialogController', ResultadoDialogController);

    ResultadoDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Resultado', 'Avaliacao'];

    function ResultadoDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Resultado, Avaliacao) {
        var vm = this;

        vm.resultado = entity;
        vm.clear = clear;
        vm.save = save;
        vm.avaliacaos = Avaliacao.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.resultado.id !== null) {
                Resultado.update(vm.resultado, onSaveSuccess, onSaveError);
            } else {
                Resultado.save(vm.resultado, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('seatApp:resultadoUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
