(function() {
    'use strict';

    angular
        .module('seatApp')
        .controller('ResultadoDeleteController',ResultadoDeleteController);

    ResultadoDeleteController.$inject = ['$uibModalInstance', 'entity', 'Resultado'];

    function ResultadoDeleteController($uibModalInstance, entity, Resultado) {
        var vm = this;

        vm.resultado = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Resultado.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
