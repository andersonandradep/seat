(function() {
    'use strict';

    angular
        .module('seatApp')
        .controller('ResultadoDetailController', ResultadoDetailController);

    ResultadoDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Resultado', 'Avaliacao'];

    function ResultadoDetailController($scope, $rootScope, $stateParams, previousState, entity, Resultado, Avaliacao) {
        var vm = this;

        vm.resultado = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('seatApp:resultadoUpdate', function(event, result) {
            vm.resultado = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
