(function() {
    'use strict';

    angular
        .module('seatApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('resultado-entity', {
            parent: 'entity',
            url: '/resultado',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'seatApp.resultado.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/resultado/resultados.html',
                    controller: 'ResultadoController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('resultado');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('resultado-detail', {
            parent: 'resultado-entity',
            url: '/resultado/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'seatApp.resultado.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/resultado/resultado-detail.html',
                    controller: 'ResultadoDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('resultado');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Resultado', function($stateParams, Resultado) {
                    return Resultado.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'resultado-entity',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('resultado-detail.edit', {
            parent: 'resultado-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/resultado/resultado-dialog.html',
                    controller: 'ResultadoDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Resultado', function(Resultado) {
                            return Resultado.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('resultado.new', {
            parent: 'resultado-entity',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/resultado/resultado-dialog.html',
                    controller: 'ResultadoDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                notaGeral: null,
                                notaPM: null,
                                notaSI: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('resultado-entity', null, { reload: 'resultado-entity' });
                }, function() {
                    $state.go('resultado-entity');
                });
            }]
        })
        .state('resultado.edit', {
            parent: 'resultado-entity',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/resultado/resultado-dialog.html',
                    controller: 'ResultadoDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Resultado', function(Resultado) {
                            return Resultado.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('resultado-entity', null, { reload: 'resultado-entity' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('resultado.delete', {
            parent: 'resultado-entity',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/resultado/resultado-delete-dialog.html',
                    controller: 'ResultadoDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Resultado', function(Resultado) {
                            return Resultado.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('resultado-entity', null, { reload: 'resultado-entity' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
