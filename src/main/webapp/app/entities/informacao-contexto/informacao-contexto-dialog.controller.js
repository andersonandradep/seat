(function() {
    'use strict';

    angular
        .module('seatApp')
        .controller('InformacaoContextoDialogController', InformacaoContextoDialogController);

    InformacaoContextoDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'InformacaoContexto', 'User'];

    function InformacaoContextoDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, InformacaoContexto, User) {
        var vm = this;

        vm.informacaoContexto = entity;
        vm.clear = clear;
        vm.save = save;
        vm.users = User.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.informacaoContexto.id !== null) {
                InformacaoContexto.update(vm.informacaoContexto, onSaveSuccess, onSaveError);
            } else {
                InformacaoContexto.save(vm.informacaoContexto, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('seatApp:informacaoContextoUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
