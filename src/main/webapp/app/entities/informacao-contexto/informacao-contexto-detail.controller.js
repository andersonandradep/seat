(function() {
    'use strict';

    angular
        .module('seatApp')
        .controller('InformacaoContextoDetailController', InformacaoContextoDetailController);

    InformacaoContextoDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'InformacaoContexto', 'User'];

    function InformacaoContextoDetailController($scope, $rootScope, $stateParams, previousState, entity, InformacaoContexto, User) {
        var vm = this;

        vm.informacaoContexto = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('seatApp:informacaoContextoUpdate', function(event, result) {
            vm.informacaoContexto = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
