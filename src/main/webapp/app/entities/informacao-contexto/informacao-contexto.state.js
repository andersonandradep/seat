(function() {
    'use strict';

    angular
        .module('seatApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('informacao-contexto', {
            parent: 'entity',
            url: '/informacao-contexto',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'seatApp.informacaoContexto.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/informacao-contexto/informacao-contextos.html',
                    controller: 'InformacaoContextoController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('informacaoContexto');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('informacao-contexto-detail', {
            parent: 'informacao-contexto',
            url: '/informacao-contexto/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'seatApp.informacaoContexto.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/informacao-contexto/informacao-contexto-detail.html',
                    controller: 'InformacaoContextoDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('informacaoContexto');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'InformacaoContexto', function($stateParams, InformacaoContexto) {
                    return InformacaoContexto.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'informacao-contexto',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('informacao-contexto-detail.edit', {
            parent: 'informacao-contexto-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/informacao-contexto/informacao-contexto-dialog.html',
                    controller: 'InformacaoContextoDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['InformacaoContexto', function(InformacaoContexto) {
                            return InformacaoContexto.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('informacao-contexto.new', {
            parent: 'informacao-contexto',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/informacao-contexto/informacao-contexto-dialog.html',
                    controller: 'InformacaoContextoDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                nomeOrganizacao: null,
                                numeroColaboradores: null,
                                numeroProjetos: null,
                                areaAtuacao: null,
                                certificacao: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('informacao-contexto', null, { reload: 'informacao-contexto' });
                }, function() {
                    $state.go('informacao-contexto');
                });
            }]
        })
        .state('informacao-contexto.edit', {
            parent: 'informacao-contexto',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/informacao-contexto/informacao-contexto-dialog.html',
                    controller: 'InformacaoContextoDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['InformacaoContexto', function(InformacaoContexto) {
                            return InformacaoContexto.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('informacao-contexto', null, { reload: 'informacao-contexto' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('informacao-contexto.delete', {
            parent: 'informacao-contexto',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/informacao-contexto/informacao-contexto-delete-dialog.html',
                    controller: 'InformacaoContextoDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['InformacaoContexto', function(InformacaoContexto) {
                            return InformacaoContexto.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('informacao-contexto', null, { reload: 'informacao-contexto' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
