(function() {
    'use strict';
    angular
        .module('seatApp')
        .factory('InformacaoContexto', InformacaoContexto);

    InformacaoContexto.$inject = ['$resource'];

    function InformacaoContexto ($resource) {
        var resourceUrl =  'api/informacao-contextos/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'getMediaColaboradores': { method: 'GET', isArray: false, url: 'api/mediaColaboradores' },
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
