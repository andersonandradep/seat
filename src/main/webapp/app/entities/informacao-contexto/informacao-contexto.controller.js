(function() {
    'use strict';

    angular
        .module('seatApp')
        .controller('InformacaoContextoController', InformacaoContextoController);

    InformacaoContextoController.$inject = ['InformacaoContexto'];

    function InformacaoContextoController(InformacaoContexto) {

        var vm = this;

        vm.informacaoContextos = [];

        loadAll();

        function loadAll() {
            InformacaoContexto.query(function(result) {
                vm.informacaoContextos = result;
                vm.searchQuery = null;
            });
        }
    }
})();
