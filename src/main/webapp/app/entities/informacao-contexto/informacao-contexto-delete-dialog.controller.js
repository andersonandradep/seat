(function() {
    'use strict';

    angular
        .module('seatApp')
        .controller('InformacaoContextoDeleteController',InformacaoContextoDeleteController);

    InformacaoContextoDeleteController.$inject = ['$uibModalInstance', 'entity', 'InformacaoContexto'];

    function InformacaoContextoDeleteController($uibModalInstance, entity, InformacaoContexto) {
        var vm = this;

        vm.informacaoContexto = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            InformacaoContexto.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
