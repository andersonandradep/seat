(function() {
    'use strict';

    angular
        .module('seatApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('resultado', {
            parent: 'app',
            url: '/resultado/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'resultado.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/avaliacao/resultado/resultado.html',
                    controller: 'ResultadoController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('resultado');
                    $translatePartialLoader.addPart('avaliacao');
                    $translatePartialLoader.addPart('dica-pergunta');
                    $translatePartialLoader.addPart('pergunta');
                    $translatePartialLoader.addPart('informacaoContexto');
                    return $translate.refresh();
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'app',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        });
    }
})();
