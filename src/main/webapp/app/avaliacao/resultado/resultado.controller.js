(function() {
    'use strict';

    angular
        .module('seatApp')
        .controller('ResultadoController', ResultadoController);

    ResultadoController.$inject = ['$scope', '$stateParams', 'Avaliacao', 'Resultado'];

    function ResultadoController($scope, $stateParams, Avaliacao, Resultado) {
    	var vm = this;
    	vm.resultado = {};
        vm.resultadoGeral = {};

        var mediaGeral = 0;

		getResultado();

		function getResultado() {
			Avaliacao.getResultado({id:$stateParams.id}, function(result) {
				vm.resultado = result;
				carregaGrafico();
			});

            Resultado.query(function(result) {
                vm.resultadoGeral = result;
                calculaMedia();
                carregaGraficoMedia();
            });

		}

        function calculaMedia() {
            var somaGeral = 0;
            var totalRespostas = 0;

            angular.forEach(vm.resultadoGeral, function(value, key) {
                somaGeral += value.notaGeral;
                totalRespostas++;
            });

            mediaGeral = somaGeral / totalRespostas;
        }

		function carregaGrafico() {
			var g = new JustGage({
				id: "gauge",
				value: vm.resultado.notaGeral,
				min: 0,
				max: 100,
				label: "Resultado da Avaliação",
				levelColors: ['#ff0000', '#ffff00', '#00ff00'],
                relativeGaugeSize: true
			});
		}

		function carregaGraficoMedia() {
            var m = new JustGage({
                id: "gaugeMedia",
                value: mediaGeral,
                min: 0,
                max: 100,
                label: "Média Geral do Sistema",
                levelColors: ['#ff0000', '#ffff00', '#00ff00'],
                relativeGaugeSize: true
            });
        }

    }

})();
