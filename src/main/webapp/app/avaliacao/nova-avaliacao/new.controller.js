(function() {
    'use strict';

    angular
        .module('seatApp')
        .controller('NewController', NewController);

    NewController.$inject = ['$scope', '$state', 'Pergunta', 'Resposta', 'Avaliacao', 'InformacaoContexto'];

    function NewController($scope, $state, Pergunta, Resposta, Avaliacao, InformacaoContexto) {
    	var vm = this;

        vm.perguntas = [];
        vm.loadAll = loadAll;
        vm.save = save;
        vm.respostas = {};

        vm.percentual = 0;

        var avaliacao = {};

        var totalPerguntas = 0;

        $scope.currentPage = 1;

        loadAll();
        criaAvaliacao();

        function loadAll() {
        	Pergunta.query(function(result) {
                vm.perguntas = result;
                angular.forEach(result, function(value, key) {
                    angular.forEach(value.perguntaDetalhadas, function(val, k) {
                        totalPerguntas++;
                        vm.respostas[val.id] = 'N';
                    });
                });
            });
        };

        function criaAvaliacao() {
        	Avaliacao.save(novaAvaliacao(), function(result) {
                avaliacao = result;
            });
        }

        function novaAvaliacao() {
            return {
                data: new Date(),
                status: "I"
            };
        };

        function save() {
        	angular.forEach(vm.respostas, function(value, key) {
                Resposta.save(criaRespostas(value, key));
        	});

            avaliacao.status = "C";
            Avaliacao.update(avaliacao);
            $state.go("resultado", avaliacao);
        }

        function criaRespostas(value, key) {
        	return {
                resposta: value,
                perguntaDetalhada: {
                    id: key
                },
                avaliacao: {
                    id: avaliacao.id
                }
            };
        }

        $scope.alterouPagina = function () {
            if ($scope.currentPage == 5) {
                $scope.exibeBotaoSalvar = true;
            }
        }

        $scope.atualizaProgressBar = function() {
            var totalRespostasSim = 0;

            angular.forEach(vm.respostas, function(value, key) {
                if (value == "S") {
                    totalRespostasSim++;
                }
            });

            var percentual = totalRespostasSim * 100 / totalPerguntas;
            vm.percentual = Math.floor(percentual);

            $('.progress-bar').css('width', percentual+'%').attr('aria-valuenow', percentual);

        }

    }

})();
