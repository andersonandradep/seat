(function() {
    'use strict';

    angular
        .module('seatApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('new', {
            parent: 'app',
            url: '/novaAvaliacao',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'assessment.new'
            },
            views: {
                'content@': {
                    templateUrl: 'app/avaliacao/nova-avaliacao/new.html',
                    controller: 'NewController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('avaliacao');
                    $translatePartialLoader.addPart('pergunta');
                    $translatePartialLoader.addPart('informacaoContexto');
                    return $translate.refresh();
                }]
            }
        });
    }
})();
