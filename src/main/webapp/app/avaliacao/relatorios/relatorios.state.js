(function() {
    'use strict';

    angular
        .module('seatApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('relatorios', {
            parent: 'app',
            url: '/relatorios',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'relatorios.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/avaliacao/relatorios/relatorios.html',
                    controller: 'RelatoriosController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
//                    $translatePartialLoader.addPart('informacaoContexto');
                    return $translate.refresh();
                }]
            }
        });
    }
})();
