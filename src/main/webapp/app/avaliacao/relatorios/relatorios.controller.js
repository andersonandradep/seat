(function() {
    'use strict';

    angular
        .module('seatApp')
        .controller('RelatoriosController', RelatoriosController);

    RelatoriosController.$inject = ['$scope', '$stateParams', 'Resultado', 'InformacaoContexto'];

    function RelatoriosController($scope, $stateParams, Resultado, InformacaoContexto) {
    	var vm = this;
    	vm.resultado = {};
    	vm.mediaColaboradores = 0;
    	vm.mediaProjetos = 0;

    	var mediaGeral = 0;
    	var mediaPM = 0;
    	var mediaSI = 0;

        getResultado();

		function getResultado() {
			Resultado.query(function(result) {
				vm.resultado = result;
				calculaMedia();
				carregaGrafico();
			});

            InformacaoContexto.query(function(result) {
                var informacoes = result;

                var totalColaboradores = 0;
                var ate10colaboradores = 0;
                var ate25colaboradores = 0;
                var mais25colaboradores = 0;

                var totalProjetos = 0;
                var umProjeto = 0;
                var doisProjetos = 0;
                var tresOuMaisProjetos = 0;

                angular.forEach(informacoes, function(value) {
                    totalColaboradores += value.numeroColaboradores;
                    totalProjetos += value.numeroProjetos;

                    function calculaFaixasColaboradores() {
                        if (value.numeroColaboradores < 11) {
                            ate10colaboradores++;
                        } else if (value.numeroColaboradores > 10 && value.numeroColaboradores < 26) {
                            ate25colaboradores++;
                        } else {
                            mais25colaboradores++;
                        }
                    }

                    function calculaFaixasProjetos() {
                        if (value.numeroProjetos == 1) {
                            umProjeto++;
                        } else if (value.numeroProjetos == 2) {
                            doisProjetos++;
                        } else {
                            tresOuMaisProjetos++;
                        }
                    }

                    calculaFaixasColaboradores();
                    calculaFaixasProjetos();
                });

                vm.mediaColaboradores = Math.floor(totalColaboradores / informacoes.length);
                vm.mediaProjetos = Math.floor(totalProjetos / informacoes.length);

                var mediaColaboradores = c3.generate({
                    bindto: '#mediaColaboradores',
                    data: {
                        columns: [
                            ['Até 10 colaboradores', ate10colaboradores],
                            ['De 10 a 25 colaboradores', ate25colaboradores],
                            ['> 25 colaboradores', mais25colaboradores]
                        ],
                        type: 'pie'
                    }
                });

                var mediaProjetos = c3.generate({
                    bindto: '#mediaProjetos',
                    data: {
                        columns: [
                            ['1 projeto', umProjeto],
                            ['2 projetos', doisProjetos],
                            ['>= 3 projetos', tresOuMaisProjetos]
                        ],
                        type: 'pie'
                    }
                });

                var mediaProjetos = c3.generate({
                    bindto: '#areasAtuacao',
                    data: {
                        columns: [
                            ['1 projeto', umProjeto],
                            ['2 projetos', doisProjetos],
                            ['>= 3 projetos', tresOuMaisProjetos]
                        ],
                        type: 'pie'
                    }
                });

            });
		}

		function calculaMedia() {
		    var somaGeral = 0;
		    var somaPM = 0;
		    var somaSI = 0;

		    var totalRespostas = 0;

            angular.forEach(vm.resultado, function(value, key) {
                somaGeral += value.notaGeral;
                somaPM += value.notaPM;
                somaSI += value.notaSI;

                totalRespostas++;
            });

            mediaGeral = somaGeral / totalRespostas;
            mediaPM = somaPM / totalRespostas;
            mediaSI = somaSI / totalRespostas;
        }

		function carregaGrafico() {
			var mg = new JustGage({
				id: "mg",
				value: mediaGeral,
				min: 0,
				max: 100,
                label: "Média Geral",
                labelFontColor: "#000000",
                levelColors: ['#ff0000', '#ffff00', '#00ff00'],
                relativeGaugeSize: true
			});
            var mpm = new JustGage({
                id: "mpm",
                value: mediaPM,
                min: 0,
                max: 100,
                label: "Média PM",
                labelFontColor: "#000000",
                levelColors: ['#ff0000', '#ffff00', '#00ff00'],
                relativeGaugeSize: true
            });
            var msi = new JustGage({
                id: "msi",
                value: mediaSI,
                min: 0,
                max: 100,
                label: "Média SI",
                labelFontColor: "#000000",
                levelColors: ['#ff0000', '#ffff00', '#00ff00'],
                relativeGaugeSize: true
            });
		}

    }

})();
