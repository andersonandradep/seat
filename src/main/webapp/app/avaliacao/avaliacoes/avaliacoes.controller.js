(function() {
    'use strict';

    angular
        .module('seatApp')
        .controller('AvaliacoesController', AvaliacoesController);

    AvaliacoesController.$inject = ['$scope', '$state', 'Avaliacao'];

    function AvaliacoesController($scope, $state, Avaliacao) {
        var vm = this;

        vm.avaliacoes = [];

        load();

        function load() {
            Avaliacao.getAvaliacoesConcluidas(function(result) {
                vm.avaliacoes = result;

                var datas = ['x'];
                var notasGerais = ['Nota Geral'];
                var notasPM = ['Nota GP'];
                var notasSI = ['Nota IS'];

                var i;
                for(i = 0; i < vm.avaliacoes.length; i++) {
                    datas.push(new Date(vm.avaliacoes[i].data));
                    notasGerais.push(vm.avaliacoes[i].resultado.notaGeral);
                    notasPM.push(vm.avaliacoes[i].resultado.notaPM);
                    notasSI.push(vm.avaliacoes[i].resultado.notaSI);
                }

                var chart = c3.generate({
                    bindto: '#avaliacoes',
                    data: {
                        x: 'x',
                        columns: [
                            datas,
                            notasGerais,
                            notasPM,
                            notasSI
                        ]
                    },
                    axis: {
                        x: {
                            type: 'timeseries',
                            tick: {
                                format: '%d/%m/%Y'
                            }
                        }
                    }
                });
            });

        }

    }
})();
