(function() {
    'use strict';

    angular
        .module('seatApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('avaliacoes', {
            parent: 'app',
            url: '/avaliacoes',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'seatApp.avaliacao.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/avaliacao/avaliacoes/avaliacoes.html',
                    controller: 'AvaliacoesController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('avaliacao');
                    return $translate.refresh();
                }]
            }
        });
    }
})();
