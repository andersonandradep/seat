(function() {
    'use strict';

    angular
        .module('seatApp')
        .controller('InformacoesContextoController', InformacoesContextoController);

    InformacoesContextoController.$inject = ['$scope', '$state', '$stateParams', 'InformacaoContexto'];

    function InformacoesContextoController($scope, $state, $stateParams, InformacaoContexto) {
        var vm = this;

        vm.save = save;
        vm.verificaAreaAtuacao = verificaAreaAtuacao;
        vm.informacoesContexto = {};

        $scope.exibeBotaoNovaAvaliacao = $stateParams.novaAvaliacao;

        load();

        function load() {
            InformacaoContexto.query(function(result) {
                vm.informacoesContexto = result[0];
            });
        }

        function verificaAreaAtuacao() {
            if (vm.informacoesContexto.areaAtuacao == 'Médico' || vm.informacoesContexto.areaAtuacao == 'Segurança') {
                vm.areaAtuacaoInvalida = true;
            } else {
                vm.areaAtuacaoInvalida = false;
            }
        }

        function save () {
            if (vm.informacoesContexto.id !== null) {
                InformacaoContexto.update(vm.informacoesContexto, onSaveSuccess);
            } else {
                InformacaoContexto.save(vm.informacoesContexto, onSaveSuccess);
            }
        }

        function onSaveSuccess () {
            if ($scope.exibeBotaoNovaAvaliacao) {
                $state.go("new");
            }
        }

    }
})();
