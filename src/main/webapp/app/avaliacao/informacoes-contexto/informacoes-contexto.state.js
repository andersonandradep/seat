(function() {
    'use strict';

    angular
        .module('seatApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('informacoes-contexto', {
            parent: 'app',
            url: '/informacoesContexto',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'seatApp.informacaoContexto.home.title'
            },
            params: {
                novaAvaliacao: null
            },
            views: {
                'content@': {
                    templateUrl: 'app/avaliacao/informacoes-contexto/informacoes-contexto.html',
                    controller: 'InformacoesContextoController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('informacaoContexto');
                    return $translate.refresh();
                }]
            }
        });
    }
})();
