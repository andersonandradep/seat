(function() {
    'use strict';

    angular
        .module('seatApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('sobre', {
            parent: 'app',
            url: '/sobre',
            data: {
                pageTitle: 'sobre.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/avaliacao/sobre/sobre.html'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
//                    $translatePartialLoader.addPart('informacaoContexto');
                    return $translate.refresh();
                }]
            }
        });
    }
})();
