(function() {
    'use strict';

    angular
        .module('seatApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
